import axios from "axios";
import { useEffect, useState } from "react";
import Loading from "../../components/loading/Loading";
import Styles from "./AboutusPage.module.css";

const AboutUsPage = () => {
  const [information, setInformation] = useState({});
  const [loading, setLoading] = useState(true);

  const init = async () => {
    try {
      const { data } = await axios.get(
        `https://react.aroosi.vip/api/data/getInfo.php`
      );
      setInformation(data.data.main);
      setLoading(false);
      console.log(information);
    } catch (error) {
      console.log("error", error);
    }
  };
  const renderDescription = () => {
    const div = document.createElement("div");
    div.innerHTML = information.description;
    return div.innerText;
  };
  const renderPage = () => {
    let returnValue = <Loading />;

    if (information && !loading) {
      returnValue = (
        <div className="row flex-column col-10 m-auto">
          <div className={`${Styles.box} col`}>
            <div className={Styles.title}>ارتباط با ما</div>
            <div>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3244.1783127247645!2d51.43679741474376!3d35.598667842104035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f91fdc94cef2209%3A0x4d479444c21b9267!2z2YXYsdqp2LIg2YXYtNin2YjYsdmHINmIINix2YjYp9mG2LTZhtin2LPbjCDZvtix2YjYp9iy!5e0!3m2!1sen!2s!4v1633016818609!5m2!1sen!2s"
                width="100%"
                height="300"
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
                title="map"
              ></iframe>
            </div>
          </div>
          <div className={`${Styles.box} col`}>
            <div className={Styles.title}>درباره ما</div>
            <div className={Styles.description}>{renderDescription()}</div>
          </div>
        </div>
      );
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, []);
  return <>{renderPage()}</>;
};

export default AboutUsPage;
