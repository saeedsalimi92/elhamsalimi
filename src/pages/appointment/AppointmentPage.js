import Styles from "./AppointmentPage.module.css";
import CalenderContent from "../../components/calender/CalenderContent";
import { useState, useEffect } from "react";
import { useToasts } from "react-toast-notifications";
import axios from "axios";
import CustomModal from "../../components/modal/CustomModal";
import Loading from "../../components/loading/Loading";
import { createOrder } from "../../services/order/createOrder";
import { updateOrder } from "../../services/order/updateOrder";
import { getReserve } from "../../services/reserve/getReserve";

let moment = require("jalali-moment");
const AppointmentPage = (props) => {
  const { history } = props;
  const attendancePrice = 1200000;
  const [selectedDate, setSelectedDate] = useState(false);
  const [date, setDate] = useState(null);
  const [prDate, setPrDate] = useState(null);
  const [loading, setLoading] = useState(true);
  const [hoursItems, setHoursItems] = useState([
    { id: 1, value: 11, reserved: false, inReserve: false },
    { id: 2, value: 12, reserved: false, inReserve: false },
    { id: 3, value: 13, reserved: false, inReserve: false },
    { id: 4, value: 14, reserved: false, inReserve: false },
    { id: 5, value: 15, reserved: false, inReserve: false },
    { id: 6, value: 16, reserved: false, inReserve: false },
    { id: 7, value: 17, reserved: false, inReserve: false },
    { id: 8, value: 18, reserved: false, inReserve: false },
    { id: 9, value: 19, reserved: false, inReserve: false },
    { id: 10, value: 20, reserved: false, inReserve: false },
    { id: 11, value: 21, reserved: false, inReserve: false },
    { id: 12, value: 22, reserved: false, inReserve: false },
  ]);
  const [hours, setHours] = useState(null);
  const [loadingHours, setLoadingHours] = useState(false);
  const [disableHours, setDisableHours] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [modalDisabled, setModalDisabled] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [persianDateView, setPersianDateView] = useState("");
  const { addToast } = useToasts();

  const [order, setOrder] = useState({});
  const [selectedTime, setSelectedTime] = useState(null);

  const differenceTwoDates = (firstDay, secondDay) => {
    const diffTime = new Date(firstDay) - new Date(secondDay);
    const diferenceDay = diffTime / (1000 * 60 * 60 * 24);
    return diferenceDay;
  };

  const [error, setError] = useState({
    name: "",
    mobile: "",
    type: "",
    payType: "",
  });
  const [formValue, setFormValue] = useState({
    name: "",
    mobile: "",
    type: "attendance",
    payType: "online",
  });

  const changeTime = (time) => {
    setSelectedTime(time);
  };

  const changeHandler = (e) => {
    if (e.target.name === "mobile") {
      if (e.target.value === "") {
        setError({
          ...error,
          mobile: "وارد کردن شماره موبایل الزامی است",
        });
      } else {
        let m = /^(\+98|98|0098|0)?(9\d{2})(\d{7})$/.exec(e.target.value);
        if (!m) {
          setError({ ...error, mobile: "شماره وارد شده صحیح نمی باشد" });
        } else {
          setError({ ...error, mobile: null });
        }
      }
    }
    if (e.target.name === "name") {
      if (e.target.value === "")
        setError({
          ...error,
          name: "وارد کردن نام الزامی است",
        });
      else if (e.target.value.length < 3) {
        setError({
          ...error,
          name: "نام و نام خانوادگی باید بیشتر از 2 کاراکتر باشد",
        });
      } else {
        setError({
          ...error,
          name: null,
        });
      }
    }
    setFormValue({ ...formValue, [e.target.name]: e.target.value });
  };
  const reserveModal = () => {
    const day = moment(date).locale("fa").format("dddd");

    if (day === "جمعه" && formValue.type === "attendance") {
      return addToast(
        "ثبت جلسه حضوری در روزهای جمعه و تعطیل امکان پذیر نیست.",
        {
          appearance: "error",
          autoDismiss: true,
        }
      );
    }
    if (formValue.type.add)
      if (
        formValue.name === "" ||
        error.mobile ||
        error.name ||
        formValue.mobile === ""
      ) {
        return addToast("نام ونام خانوادگی و شماره موبایل اجباری است", {
          appearance: "error",
          autoDismiss: true,
        });
      }
    if (!selectedDate || !selectedTime)
      return addToast("انتخاب تاریخ و زمان رزرو اجباری است", {
        appearance: "error",
        autoDismiss: true,
      });
    setShowModal(true);
  };

  const onSelectedDate = async (date) => {
    if (!date || date === "") return setSelectedDate(false);
    setLoadingHours(true);
    setDate(date);
    const persianDate = moment(date).locale("fa").format("YYYY/M/D");
    setPrDate(moment(date).locale("fa").format("YYYY/M/D"));
    setSelectedDate(true);
    setSelectedTime(false);
    setPersianDateView(moment(date).locale("fa").format("dddd D MMMM YYYY"));
    const today = moment(new Date()).locale("fa").format("YYYY/M/D");
    const day = moment(date).locale("fa").format("dddd");

    const diff = differenceTwoDates(persianDate, today);
    if (diff >= 0) {
      setDisableHours(false);
    } else {
      setDisableHours(true);
    }
    if (persianDate === today) {
      setHours(new Date().getHours());
    } else {
      setHours(null);
    }
    if (day !== "جمعه") {
      try {
        const pr_day = moment(date).locale("fa").format("DD");
        const pr_month = moment(date).locale("fa").format("MM");
        const ar_day = new Intl.DateTimeFormat("ar-TN-u-ca-islamic", {
          day: "numeric",
        }).format(date);
        const ar_month = new Intl.DateTimeFormat("ar-TN-u-ca-islamic", {
          month: "numeric",
        }).format(date);
        const getHoliday = await axios.get(
          `https://farsicalendar.com/api/sh,ic/${pr_day},${
            +ar_day - 1
          }/${pr_month},${ar_month}`
        );
        setDisableHours(false);
        let isHoliday = false;
        if (getHoliday.data.values) {
          const holiday = getHoliday.data.values.find((item) => item.dayoff);
          if (holiday) {
            setDisableHours(true);
            isHoliday = true;
          }
        }
        if (!isHoliday) {
          const getReserveDay = await axios.get(
            `https://react.aroosi.vip/api/reserve/get.php`,

            {
              params: {
                date: moment(date).locale("fa").format("YYYY/M/D"),
              },
            },
            {
              headers: {
                "Access-Control-Allow-Origin": "*",
              },
            }
          );
          const items = [...hoursItems];
          const filteredReservedHours = items.map((item) => {
            item.reserved = false;
            item.inReserve = false;
            getReserveDay.data.data.forEach((reserve) => {
              if (parseInt(reserve.time) === item.value) {
                item.reserved = true;
              }
            });
            return item;
          });

          setHoursItems(filteredReservedHours);
        }

        setLoadingHours(false);
      } catch {
        setLoadingHours(false);
      }
    } else {
      setLoadingHours(false);
      setDisableHours(true);
    }
  };
  const checkTrueTimeForAttendance = () => {
    if (parseInt(selectedTime) < 20 && parseInt(selectedTime) > 13) {
      return true;
    }
    return false;
  };
  const onAccept = async () => {
    try {
      setModalDisabled(true);
      setModalLoading(true);
      const hasReserveInTime = await getReserve(prDate, selectedTime);
      if (
        hasReserveInTime.data.data.length > 0 ||
        (hasReserveInTime.data.data && hasReserveInTime.data.data.id)
      ) {
        addToast("امکان ثبت جلسه مشاوره برای این ساعت وجود ندارد. ", {
          appearance: "error",
          autoDismiss: true,
        });
        setModalDisabled(false);
        setModalLoading(false);
      } else {
        if (checkTrueTimeForAttendance()) {
          if (formValue.payType === "online") {
            const orderData = {
              price: attendancePrice,
              type:
                formValue.type === "attendance"
                  ? "حضوری"
                  : formValue.type === "phone"
                  ? "تلفنی"
                  : "اینترنتی",
              name: formValue.name,
              phone: formValue.mobile,
              date: moment(date).locale("fa").format("YYYY/M/D"),
              hours: selectedTime,
              payType: "درگاه",
              code: 0,
            };
            const createOrderData = await createOrder(orderData);
            setOrder(createOrderData.data.data);
            const createPaymentData = await axios.get(
              `https://react.aroosi.vip/api/user/createPayment.php`,

              {
                params: {
                  price: attendancePrice,
                  desc:
                    formValue.type === "attendance"
                      ? "حضوری"
                      : formValue.type === "phone"
                      ? "تلفنی"
                      : "اینترنتی",
                  name: formValue.name,
                  phone: formValue.mobile,
                  id: createOrderData.data.data.id,
                },
              },
              {
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Content-Type": "application/x-www-form-urlencoded",
                },
              }
            );
            const params = {
              id: createOrderData.data.data.id,
              res_id: createPaymentData.data.id,
            };
            await updateOrder(params);
            addToast("درحال انتقال به سایت پذیرنده ...", {
              appearance: "success",
              autoDismiss: true,
            });
            setModalDisabled(false);
            setModalLoading(false);
            window.location.href = createPaymentData.data.link;
          } else {
            const rand = () => Math.random(0).toString(36).substr(2);
            const token = (length) =>
              (rand() + rand() + rand() + rand()).substr(0, length);
            const params = {
              price: attendancePrice,
              type:
                formValue.type === "attendance"
                  ? "حضوری"
                  : formValue.type === "phone"
                  ? "تلفنی"
                  : "اینترنتی",
              name: formValue.name,
              phone: formValue.mobile,
              date: moment(date).locale("fa").format("YYYY/M/D"),
              hours: selectedTime,
              payType: "کارت به کارت",
              res_id: token(40),
              code: 0,
            };
            const createOrderData = await createOrder(params);
            setOrder(createOrderData.data.data);
            history.push(
              `/result?pay=cart&order_id=${createOrderData.data.data.order_id}&track_id=123456&status=200&res_id=${createOrderData.data.data.res_id}`
            );
          }
        } else {
          addToast(
            "ثبت جلسه حضوری فقط از ساعت 14 الی 20 امکان پذیر می باشد. ",
            {
              appearance: "error",
              autoDismiss: true,
            }
          );
          setModalDisabled(false);
          setModalLoading(false);
        }
      }
      setShowModal(false);
    } catch (error) {
      console.log("error", error);
    }
  };

  const renderPage = () => {
    let returnValue = <Loading />;

    if (!loading) {
      returnValue = (
        <div
          className={`${Styles.box} row flex-column col-12 col-md-10 m-auto `}
        >
          <div className={`col`}>
            <div className={Styles.title}>نوبت دهی مشاوره</div>
          </div>
          <div className={Styles.tipBox}>
            <div className={Styles.tipTitle}>
              لطفا قبل از رزور وقت به نکات زیر توجه نمایید.
            </div>
            <div className={Styles.tip}>
              مدت زمان هر جلسه مشاوره <b>45</b> دقیقه می باشد.
            </div>
            <div className={Styles.tip}>
              ثبت جلسه حضوری فقط از ساعت <b>14</b> الی ساعت <b>19</b> امکان پذیر
              می باشد.
            </div>
            <div className={Styles.tip}>
              هزینه هر جلسه مشاوره (حضوری ، تلفنی ، اینترنتی) <b>120</b> هزار
              تومان می باشد.
            </div>
            <div className={Styles.tip}>
              در صورت داشتن تست، هزینه هر تست به صورت جداگانه حساب می گردد.
            </div>

            <div className={Styles.tip}>
              پس از نهایی شدن رزرو جلسه شما، پیامک تاییدیه ثبت رزور برای شماره
              موبایل وارد شده ارسال می گردد.
            </div>
            <div className={Styles.tip}>
              ارسال پیامک ثبت جلسه و یا تماس از طرف مرکز به منزله تایید ثبت جلسه
              مشاوره شما می باشد.
            </div>
            <div className={Styles.tip}>
              اطلاعات خواسته شده را به صورت صحیح وارد نمایید.
            </div>
            <div className={Styles.tip}>
              در صورت عدم حضور در جلسه مشاوره مبلغ پرداختی به هیچ وجه قابل
              بازگشت نمی باشد.
            </div>
            <div className={Styles.tip}>
              در صورت پرداخت کارت به کارت، رسید هزینه واریزی به همراه نام و نام
              خانوادگی و شماره موبایل و ساعت و روز جلسه رزرو شده را به واتساپ
              این شماره <b>09380017242</b> ارسال نمایید تا جلسه برای شما رزرو
              گردد.
            </div>
            <div className={Styles.tip}>
              در صورت انتخاب پرداخت به صورت کارت به کارت جلسه برای شما به مدت{" "}
              <b> 60 </b>
              دقیقه رزور شده و در صورت عدم پرداخت هزینه و اطلاع رسانی رزرو جلسه
              شما لغو می گردد.
            </div>
            <div className={Styles.tip}>
              شماره کارت جهت پرداخت به صورت کارت به کارت <b>6221061083344832</b>{" "}
              به نام <b>الهام سلیمی</b> می باشد.
            </div>
          </div>
          <div className={`${Styles.radioBox} row justify-content-center`}>
            <div className="col col-sm-12 col-md-12 col-12 mb-2 text-gray  ">
              مشخصات:
            </div>
            <div className="col col-sm-12 col-md-6 col-12 mb-2 ">
              <label className={Styles.inputTitle}>
                نام و نام خانوادگی
                <span className={Styles.inputSubtitle}> (به فارسی) </span>
              </label>
              <input
                className={`${Styles.input} ${
                  error.name && Styles.errorInput
                } ${
                  !error.name && !error.mobile && formValue.name
                    ? Styles.successInput
                    : ""
                }`}
                type="text"
                onChange={changeHandler}
                placeholder="نام و نام خانوادگی"
                name="name"
                value={formValue.name}
              />
              {error.name && <div className={Styles.error}>{error.name}</div>}
            </div>
            <div className="col col-sm-12 col-md-6 col-12">
              <label className={Styles.inputTitle}> شماره موبایل </label>
              <input
                className={`${Styles.input} ${
                  error.mobile && Styles.errorInput
                } ${
                  !error.name && !error.mobile && formValue.mobile
                    ? Styles.successInput
                    : ""
                }`}
                type="text"
                onChange={changeHandler}
                placeholder="شماره موبایل"
                name="mobile"
                value={formValue.mobile}
              />
              {error.mobile && (
                <div className={Styles.error}>{error.mobile}</div>
              )}
            </div>
          </div>
          <div className={`${Styles.radioBox} row justify-content-center mt-4`}>
            <div className="col col-sm-12 col-md-6 col-12 mb-2 text-gray ">
              نوع مشاوره:
            </div>
            <div className="col col-sm-12 col-md-6 col-12">
              <label htmlFor="attendance" className={Styles.label}>
                حضوری
              </label>
              <input
                className={Styles.labelInput}
                id="attendance"
                type="radio"
                name="type"
                value="attendance"
                checked={formValue.type === "attendance"}
                onChange={changeHandler}
              />
              <label htmlFor="phone" className={Styles.label}>
                تلفنی
              </label>
              <input
                className={Styles.labelInput}
                id="phone"
                type="radio"
                name="type"
                value="phone"
                checked={formValue.type === "phone"}
                onChange={changeHandler}
              />
              <label htmlFor="internet" className={Styles.label}>
                اینترنتی (تماس تصویری یا صوتی )
              </label>
              <input
                className={Styles.labelInput}
                id="internet"
                type="radio"
                name="type"
                value="internet"
                checked={formValue.type === "internet"}
                onChange={changeHandler}
              />
            </div>
          </div>
          <div className={`${Styles.radioBox} row justify-content-center mt-4`}>
            <div className="col col-sm-12 col-md-12 col-12 mb-2 text-gray ">
              تاریخ:
            </div>
            <div className="col col-sm-12 col-md-12 col-12">
              <CalenderContent onSelectedDate={onSelectedDate} />
            </div>
          </div>

          {loadingHours && (
            <div className={Styles.loading}>درحال دریافت اطلاعات...</div>
          )}

          {selectedDate && !disableHours && !loadingHours && (
            <div
              className={`${Styles.btnBox} row justify-content-center text-center ma-0`}
            >
              <div className="col col-sm-12 col-md-12 col-12 mb-2 text-gray text-right">
                ساعت:
              </div>
              {hoursItems.map((item) => {
                return (
                  <div key={item.id} className="col col-md-4 col-lg-2 col-6">
                    <button
                      onClick={() => changeTime(item.value)}
                      type="button"
                      className={`${Styles.btn} ${
                        parseInt(selectedTime) === item.value &&
                        Styles.activeBtn
                      }
                       ${item.inReserve && Styles.redBtn} btn btn-primary`}
                      disabled={
                        (hours && hours >= item.value) ||
                        disableHours ||
                        item.reserved ||
                        item.inReserve
                      }
                    >
                      {`${item.value} - ${item.value}:45`}
                    </button>
                  </div>
                );
              })}
            </div>
          )}
          {disableHours && !loadingHours && (
            <div className={Styles.holiday}>در این روز مرکز تعطیل می باشد </div>
          )}

          <div className={`${Styles.radioBox} row justify-content-center`}>
            <div className="col  col-sm-12 col-md-6 col-12 mb-2 text-gray">
              طریقه پرداخت:
            </div>
            <div className="col  col-sm-12 col-md-6 col-12 ">
              <label htmlFor="online" className={Styles.label}>
                درگاه اینترنتی (ID PAY)
              </label>
              <input
                className={Styles.labelInput}
                id="online"
                type="radio"
                name="payType"
                value="online"
                checked={formValue.payType === "online"}
                onChange={changeHandler}
              />
              <label htmlFor="kart" className={Styles.label}>
                کارت به کارت
              </label>
              <input
                className={Styles.labelInput}
                id="kart"
                type="radio"
                name="payType"
                value="kart"
                checked={formValue.payType === "cart"}
                onChange={changeHandler}
              />
            </div>
          </div>

          <div className={`${Styles.radioBox} row justify-content-center`}>
            <div className="col col-12">
              <div className={`${Styles.btnBox} text-center`}>
                <button
                  type="button"
                  className={`${Styles.btn} btn btn-success`}
                  onClick={() => reserveModal()}
                  disabled={
                    error.name ||
                    error.mobile ||
                    formValue.name === "" ||
                    formValue.mobile === "" ||
                    !selectedDate ||
                    !selectedTime
                  }
                >
                  نهایی کردن نوبت شما
                </button>
              </div>
            </div>
          </div>
          <CustomModal
            show={showModal}
            onHide={() => setShowModal(false)}
            hours={selectedTime}
            price={attendancePrice}
            name={formValue.name}
            payType={formValue.payType}
            type={formValue.type}
            phone={formValue.phone}
            persianDate={persianDateView}
            onAccept={onAccept}
            onDisabled={modalDisabled}
            onLoading={modalLoading}
          />
        </div>
      );
    }

    return returnValue;
  };

  useEffect(() => {
    const init = setTimeout(() => {
      setLoading(false);
    }, 500);
    return () => {
      clearTimeout(init);
    };
  }, []);

  return <>{renderPage()}</>;
};
export default AppointmentPage;
