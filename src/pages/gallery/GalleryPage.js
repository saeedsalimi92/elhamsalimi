import Styles from "./GalleryPage.module.css";
import logo0 from "../../assets/images/01.jpeg";
import logo1 from "../../assets/images/02.jpeg";
import logo2 from "../../assets/images/03.jpeg";
import logo3 from "../../assets/images/04.jpeg";

import { useState } from "react";
import { LightBox } from "react-lightbox-pack"; // <--- Importing LightBox Pack
import "react-lightbox-pack/dist/index.css"; // <--- Importing Sample JSON Data

const GalleryPage = () => {
  const data = [
    {
      id: 1,
      image: logo0,
      title: "الهام سلیمی",
      description: "مرکز مشاوره و خدمات روانشناختی پرواز",
    },
    {
      id: 2,
      image: logo1,
      title: "الهام سلیمی",
      description: "مرکز مشاوره و خدمات روانشناختی پرواز",
    },
    {
      id: 3,
      image: logo2,
      title: "الهام سلیمی",
      description: "مرکز مشاوره و خدمات روانشناختی پرواز",
    },
    {
      id: 4,
      image: logo3,
      title: "الهام سلیمی",
      description: "مرکز مشاوره و خدمات روانشناختی پرواز",
    },
  ];
  const [toggle, setToggle] = useState(false);
  const [sIndex, setSIndex] = useState(0);

  // Handler
  const lightBoxHandler = (state, sIndex) => {
    setToggle(state);
    setSIndex(sIndex);
  };

  return (
    <div className={`${Styles.box} row col-10 m-auto flex-column`}>
      <div className={`${Styles.title} col`}>گالری</div>
      <div className="col">
        <div className="row text-center ">
          {data.map((item, index) => (
            <div
              className={`${Styles.item} col-12 col-sm-9 col-md-6 col-lg-4 col-xl-3 col-xxl-3`}
              key={item.id}
            >
              <img
                className={Styles.image}
                src={item.image}
                alt={item.title}
                style={{ width: "100%" }}
                onClick={() => {
                  lightBoxHandler(true, index);
                }}
              />
            </div>
          ))}

          <LightBox
            state={toggle}
            event={lightBoxHandler}
            data={data}
            imageWidth="60vw"
            imageHeight="70vh"
            thumbnailHeight={50}
            thumbnailWidth={50}
            setImageIndex={setSIndex}
            imageIndex={sIndex}
          />
        </div>
        {/*  <div className={`${Styles.item} col-3`}>image</div>
          <div className={`${Styles.item} col-3`}>image</div>
          <div className={`${Styles.item} col-3`}>image</div>
          <div className={`${Styles.item} col-3`}>image</div>*/}
      </div>
    </div>
  );
};
export default GalleryPage;
