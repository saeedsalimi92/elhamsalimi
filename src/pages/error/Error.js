import { Link } from "react-router-dom";

const ErrorPage = ({ status }) => {
  return (
    <div
      className={`col-12 m-auto col-sm-12 col-xl-10 col-md-12 col-xxl-10 col-lg-10 `}
    >
      <div className="row justify-content-center ">
        <div className="col col-12 text-center">
          <div className="mb-3 mt-5 pt-5" dir="ltr">
            <h3>{status} خطای</h3>
          </div>
          <div className="mb-3">
            <h5>
              {status === 404
                ? "صفحه مورد نظر یافت نشد"
                : "مشکلی پیش آمده است لطفا مجدد امتحان فرمایید"}
            </h5>
          </div>
          <div className="mb-5 pb-2">
            <button type="button" className="btn btn-primary">
              <Link to="/"> بازگشت به خانه</Link>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ErrorPage;
