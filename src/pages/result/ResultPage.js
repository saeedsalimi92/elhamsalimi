import { useEffect, useState } from "react";

import axios from "axios";
import Styles from "./ResultPage.module.css";
import Loading from "../../components/loading/Loading";
import { useLocation } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { createReserve } from "../../services/reserve/createReserve";
import { getOrder } from "../../services/order/getOrder";
import { updateOrder } from "../../services/order/updateOrder";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const ResultPage = (params) => {
  let query = useQuery();

  const [loading, setLoading] = useState(true);
  const [success, setSuccess] = useState(false);
  const [order, setOrder] = useState({});

  const [resId, setResId] = useState(null);
  const [trackId, setTrackId] = useState(null);
  const { addToast } = useToasts();

  const init = async () => {
    setResId(query.get("id"));
    const pay = query.get("pay");
    const track_id = query.get("track_id");
    setTrackId(track_id);
    try {
      const order_id = query.get("order_id");
      const getOrderData = await getOrder(order_id);
      setSuccess(true);
      setOrder(getOrderData.data.data);

      if (pay === "cart") {
        params = {
          order_id: getOrderData.data.data.order_id,
          name: getOrderData.data.data.name,
          phone: getOrderData.data.data.phone,
          date: getOrderData.data.data.date,
          time: getOrderData.data.data.hours,
          payType: "کارت به کارت",
          isPay: "پرداخت نشده",
          type:
            getOrderData.data.data.type === "attendance"
              ? "حضوری"
              : getOrderData.data.data.type === "phone"
              ? "تلفنی"
              : "اینترنتی",
        };
        await createReserve(params);
        setSuccess(true);
        setLoading(false);
      } else {
        if (parseInt(getOrderData.data.data.id) > 0) {
          const verifyData = await axios.get(
            `https://react.aroosi.vip/api/user/verifyPayment.php`,
            {
              params: {
                id: getOrderData.data.data.id,
              },
            },
            {
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "*",
              },
            }
          );
          if (verifyData.data.error_code) {
            addToast(verifyData.data.error_message, {
              appearance: "error",
              autoDismiss: true,
            });
          } else {
            const checkPayment = await axios.get(
              `https://react.aroosi.vip/api/user/inquiryPayment.php`,
              {
                params: {
                  id: getOrderData.data.data.id,
                },
              },
              {
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Content-Type": "*",
                },
              }
            );
            const statusCode = parseInt(checkPayment.data.status);
            let errorText = "";

            switch (statusCode) {
              case 1:
                errorText = "پرداحت انجام نشده است";
                break;
              case 2:
                errorText = "پرداحت نا موفق بوده است";
                break;
              case 3:
                errorText = "خطا رخ داده است";
                break;
              case 4:
                errorText = "بلوکه شده است";
                break;
              case 5:
                errorText = "برگشت به پرداخت کننده";
                break;
              case 6:
                errorText = "برگشت خورده سیستمی";
                break;
              case 7:
                errorText = "انصراف از پرداخت";
                break;
              case 8:
                errorText = "به درگاه پرداخت منتقل شده است";
                break;
              case 10:
                errorText = "درانتظار تایید پرداخت";
                setSuccess(true);
                break;
              case 100:
                errorText = "پرداخت تایید شده است";
                setSuccess(true);
                break;
              case 101:
                errorText = "پرداخت قبلا تایید شده است";
                break;
              case 200:
                errorText = "به دریافت کننده واریز شد";
                setSuccess(true);
                break;
              default:
                errorText = "پرداخت نا موفق بوده است";
            }

            let orderData = {
              id: getOrderData.data.data.id,
              track_id: checkPayment.data.track_id || track_id,
              verify_track_id:
                statusCode === 200 || statusCode === 100 || statusCode === 10
                  ? checkPayment.data.payment.track_id
                  : resId,
              payment_id: checkPayment.data.id || resId,
              card_no: checkPayment.data?.payment?.card_no || "",
              verify: checkPayment.data.verify_time || "",
              response_text: errorText,
              status: 1,
            };
            await updateOrder(orderData);
            if (statusCode === 100 || statusCode === 200) {
              const params = {
                order_id: checkPayment.data.order_id,
                name: getOrderData.data.data.name,
                phone: getOrderData.data.data.phone,
                date: getOrderData.data.data.date,
                time: getOrderData.data.data.hours,
                isPay: "پرداخت شده",
                payType: "درگاه",
                type:
                  getOrderData.data.data.type === "attendance"
                    ? "حضوری"
                    : getOrderData.data.data.type === "phone"
                    ? "تلفنی"
                    : "اینترنتی",
              };
              await createReserve(params);
            } else {
              addToast("متاسفانه مشکلی پیش آمده است.", {
                appearance: "error",
                autoDismiss: true,
              });
            }
            setLoading(false);
          }
        }
      }
      setLoading(false);
    } catch (error) {
      addToast(error.response.data.message, {
        appearance: "error",
        autoDismiss: true,
      });
      setLoading(false);
    }
  };

  const renderPage = () => {
    let returnValue = <Loading />;

    if (!loading) {
      returnValue = (
        <div
          className={`${Styles.box} col-12 m-auto col-sm-12 col-xl-10 col-md-12 col-xxl-10 col-lg-10 row `}
        >
          <div className="col col-12">
            <div className="row">
              <div className={`${Styles.title} col col-12`}> نتیجه پرداخت </div>
            </div>
            <div className="row justify-content-center">
              <div
                className={`${Styles.result} ${
                  success && Styles.success
                } col col-12`}
              >
                <div className={`${Styles.item} row`}>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}> نتیجه پرداخت: </span>
                    {success ? "موفق" : "ناموفق"}
                  </div>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}>کد رهگیری: </span>
                    {trackId}
                  </div>
                </div>
                <div className={`${Styles.item} row`}>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}> نام و نام خانوادگی: </span>
                    {order.name}
                  </div>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}>شماره موبایل: </span>
                    {order.phone}
                  </div>
                </div>
                <div className={`${Styles.item} row`}>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}> تاریخ جلسه مشاوره: </span>
                    {order.date}
                  </div>
                  <div className="col col-12 col-sm-12 col-md-6 mb-3">
                    <span className={Styles.span}>ساعت جلسه مشاوره: </span>
                    {order.hours}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 100);
    return () => {
      clearTimeout(initial);
    };
  }, []);

  return <>{renderPage()}</>;
};
export default ResultPage;
