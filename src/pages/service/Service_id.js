import BreadCrumb from "../../components/Breadcrumb/BreadCrumb";
import DetailPage from "../../components/detailPage/DetailPage";
import axios from "axios";
import { useEffect, useState } from "react";
import Loading from "../../components/loading/Loading";
import ErrorPage from "../error/Error";

const Service_id = (props) => {
  let id = props.match.params.id;
  const [service, setService] = useState({});
  const [allService, setAllService] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const init = async () => {
    try {
      const allData = await axios.get(
        `https://react.aroosi.vip/api/service/item/getAll.php?page=1`
      );
      const { data } = await axios.get(
        `https://react.aroosi.vip/api/service/item/get.php?id=${id}`
      );

      setService(data.data);
      setAllService(allData.data.data);
      setLoading(false);
    } catch {
      setService(null);
      setError(true);
      setLoading(false);
    }
  };
  const renderPage = () => {
    let returnValue = <Loading />;

    if (service && !loading && !error) {
      returnValue = (
        <div>
          <BreadCrumb name="خدمات" routeName="service" item={service} />
          <DetailPage
            name="خدمات"
            routeName="service"
            item={service}
            items={allService}
          />
        </div>
      );
    }
    if (!service && !loading && error) {
      returnValue = <ErrorPage status={404} />;
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, [id]);

  return <>{renderPage()}</>;
};
export default Service_id;
