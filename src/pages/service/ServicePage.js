import { useEffect, useState } from "react";
import ServiceItem from "../../components/services/serviceBoxComponent/ServiceItem/ServiceItem";

import axios from "axios";
import Styles from "./ServicePage.module.css";
import Loading from "../../components/loading/Loading";
import { useLocation } from "react-router-dom";

import PaginationComponent from "../../components/pagination/PaginationComponent";
import ErrorPage from "../error/Error";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const ServicePage = (params) => {
  let query = useQuery();
  let max = 1;
  let pageId = query.get("page") || 1;
  if (Number(pageId) > max) {
    pageId = 1;
  }
  const [currentPage, setCurrentPage] = useState(pageId);
  const [paginates, setPaginates] = useState({});
  const [services, setServices] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const init = async () => {
    try {
      const { data } = await axios.get(
        `https://react.aroosi.vip/api/service/item/getAll.php?page=${pageId}`
      );
      setServices(data.data);
      setLoading(false);
      setPaginates({
        max: 1,
        current: currentPage,
      });
      return data;
    } catch {
      setServices(null);
      setError(true);
      setLoading(false);
    }
  };
  const changePaginate = (page) => {
    setCurrentPage(page);
  };
  const renderPage = () => {
    let returnValue = <Loading />;

    if (services && !loading && !error) {
      returnValue = (
        <div
          className={`${Styles.box} col-12 m-auto col-sm-12 col-xl-10 col-md-12 col-xxl-10 col-lg-10`}
        >
          <div className={Styles.title}>خدمات</div>
          <div className="row justify-content-center">
            {services.map((service) => {
              return (
                <div
                  className="col col-md-6 col-lg-4 col-xxl-4 col-xl-4 col-sm-9 col-12"
                  key={service.id}
                >
                  <ServiceItem service={service} />
                </div>
              );
            })}
          </div>
          <div className="row col-10 m-auto">
            <PaginationComponent
              changePaginate={changePaginate}
              {...params}
              {...paginates}
              current={currentPage}
            />
          </div>
        </div>
      );
    }
    if (!services && !loading && error) {
      returnValue = <ErrorPage status={404} />;
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, [currentPage]);

  return <>{renderPage()}</>;
};
export default ServicePage;
