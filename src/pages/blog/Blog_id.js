import BreadCrumb from "../../components/Breadcrumb/BreadCrumb";
import DetailPage from "../../components/detailPage/DetailPage";
import axios from "axios";
import { useEffect, useState } from "react";
import Loading from "../../components/loading/Loading";
import ErrorPage from "../error/Error";

const Blog_id = (props) => {
  let id = props.match.params.id;
  const [blog, setBlog] = useState({});
  const [allBlog, setAllBlog] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const init = async () => {
    try {
      const allData = await axios.get(
        `https://react.aroosi.vip/api/blog/item/getAll.php?page=1`
      );
      const { data } = await axios.get(
        `https://react.aroosi.vip/api/blog/item/get.php?id=${id}`
      );
      setBlog(data.data);

      setAllBlog(allData.data.data);
      setLoading(false);
      setError(false);
    } catch {
      setError(true);
      setBlog(null);
      setLoading(false);
    }
  };
  const renderPage = () => {
    let returnValue = <Loading />;
    if (!blog && !loading && error) {
      returnValue = <ErrorPage status={404} />;
    }
    if (blog && !loading && !error) {
      returnValue = (
        <div>
          <BreadCrumb name="بلاگ" routeName="blog" item={blog} />
          <DetailPage
            name="بلاگ"
            routeName="blog"
            item={blog}
            items={allBlog}
          />
        </div>
      );
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, [id]);

  return <>{renderPage()}</>;
};
export default Blog_id;
