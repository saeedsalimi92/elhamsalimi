import BlogItem from "../../components/blog/BlogBoxComponent/BlogItem/BlogItem";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import Styles from "./BlogPage.module.css";
import Loading from "../../components/loading/Loading";
import PaginationComponent from "../../components/pagination/PaginationComponent";
import ErrorPage from "../error/Error";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}
const BlogPage = (params) => {
  let query = useQuery();
  let max = 1;
  let pageId = query.get("page") || 1;
  if (Number(pageId) > max) {
    pageId = 1;
  }
  const [currentPage, setCurrentPage] = useState(pageId);
  const [blog, setBlog] = useState([]);
  const [paginates, setPaginates] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const init = async () => {
    try {
      const { data } = await axios.get(
        `https://react.aroosi.vip/api/blog/item/getAll.php?page=${currentPage}`
      );
      setBlog(data.data);

      setPaginates({
        max: 1,
        current: currentPage,
      });
      setLoading(false);
      setError(false);
    } catch (error) {
      setError(true);
      setBlog(null);
      setLoading(false);
    }
  };
  const changePaginate = (page) => {
    setCurrentPage(page);
  };
  const renderPage = () => {
    let returnValue = <Loading />;

    if (blog && !loading && !error) {
      returnValue = (
        <div
          className={`${Styles.box} col-12 m-auto col-sm-12 col-xl-10 col-md-12 col-xxl-10 col-lg-10 `}
        >
          <div className={Styles.title}>بلاگ</div>
          <div className="row justify-content-center">
            {blog.map((item) => {
              return (
                <div
                  className="col col-md-6 col-lg-4 col-xxl-4 col-xl-4 col-sm-9 col-12"
                  key={item.id}
                >
                  <BlogItem item={item} />
                </div>
              );
            })}
          </div>
          <div className="row col-10 m-auto">
            <PaginationComponent
              current={currentPage}
              {...paginates}
              {...params}
              changePaginate={changePaginate}
            />
          </div>
        </div>
      );
    }
    if (!blog && !loading && error) {
      returnValue = <ErrorPage status={404} />;
    }

    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, [currentPage]);

  return <>{renderPage()}</>;
};
export default BlogPage;
