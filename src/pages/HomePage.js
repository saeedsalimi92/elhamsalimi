import SwiperContainer from "../components/Swiper/SwiperComponent";
import axios from "axios";
import { useEffect, useState } from "react";
import ServiceComponentBox from "../components/services/serviceBoxComponent/ServiceComponentBox";
import BlogComponentBox from "../components/blog/BlogBoxComponent/BlogComponentBox";
import AboutComponentBox from "../components/about/AboutBoxComponent/AboutComponentBox";
import Loading from "../components/loading/Loading";

const HomePage = () => {
  const [sliders, setSliders] = useState([]);
  const [services, setServices] = useState([]);
  const [aboutUs, setAboutUs] = useState({});
  const [blog, setBlog] = useState([]);
  const [loading, setLoading] = useState(true);
  const init = async () => {
    try {
      const { data } = await axios.get(
        "https://react.aroosi.vip/api/data/get.php"
      );
      setSliders(data.data.slider_items);
      setServices(data.data.service_items);
      setBlog(data.data.blog_items);
      setAboutUs(data.data.main);
      setLoading(false);
    } catch (error) {
      console.log("error", error);
    }
  };
  const renderPage = () => {
    let value = <Loading />;

    if (sliders && services && blog && !loading) {
      value = (
        <div>
          <SwiperContainer sliders={sliders} />
          <ServiceComponentBox services={services} />
          <AboutComponentBox aboutUs={aboutUs} />
          <BlogComponentBox blog={blog} />
        </div>
      );
    }
    return value;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, []);
  return <>{renderPage()}</>;
};
export default HomePage;
