import { Route, Switch } from "react-router-dom";
import "./App.css";
import Layout from "./layout/Layout";
import HomePage from "./pages/HomePage";
import BlogPage from "./pages/blog/BlogPage";
import ServicePage from "./pages/service/ServicePage";
import Blog_id from "./pages/blog/Blog_id";
import Service_id from "./pages/service/Service_id";
import GalleryPage from "./pages/gallery/GalleryPage";
import AboutUsPage from "./pages/aboutUs/AboutUsPage";
import AppointmentPage from "./pages/appointment/AppointmentPage";
import LoginPage from "./pages/login/LoginPage";
import ResultPage from "./pages/result/ResultPage";
import ErrorPage from "./pages/error/Error";
import { ToastProvider } from "react-toast-notifications";

function App() {
  return (
    <ToastProvider>
      <Layout>
        <div className="main-view">
          <Switch>
            <Route path="/blog/:id" component={Blog_id} />
            <Route path="/blog" component={BlogPage} />
            <Route path="/service/:id" component={Service_id} />
            <Route path="/service" component={ServicePage} />
            <Route path="/about-us" component={AboutUsPage} />
            <Route path="/gallery" component={GalleryPage} />
            <Route path="/login" component={LoginPage} />
            <Route path="/appointment" component={AppointmentPage} />
            <Route path="/result" component={ResultPage} />
            <Route path="/" exact component={HomePage} />
            <Route path="*" component={() => <ErrorPage status={404} />} />
          </Switch>
        </div>
      </Layout>
    </ToastProvider>
  );
}

export default App;
