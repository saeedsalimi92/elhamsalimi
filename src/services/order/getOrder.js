import http from "../axios/http";
import qs from "qs";

export function getOrder(data) {
  return http.get("user/order/get.php?order_id=" + data);
}
