import http from "../axios/http";
import qs from "qs";

export function updateOrder(data) {
  const params = qs.stringify(data);
  return http.post("user/order/updateOrder.php", params);
}
