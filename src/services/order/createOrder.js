import http from "../axios/http";
import qs from "qs";

export function createOrder(data) {
  const params = qs.stringify(data);
  return http.post("user/order/createOrder.php", params);
}
