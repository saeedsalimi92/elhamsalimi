import http from "../axios/http";

export function getReserve(date, time) {
  return http
    .get(`reserve/get.php?date=${date}&time=${time}`)
    .then((res) => res);
}
