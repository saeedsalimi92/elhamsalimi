import http from "../axios/http";
import qs from "qs";

export function createReserve(data) {
  const params = qs.stringify(data);
  return http.post("reserve/create.php", params);
}
