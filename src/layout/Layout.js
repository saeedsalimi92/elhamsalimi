import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import { useEffect, useState } from "react";
import axios from "axios";
import Loading from "../components/loading/Loading";

const Layout = ({ children }) => {
  const [info, setInfo] = useState({});

  const [loading, setLoading] = useState(true);

  const init = async () => {
    try {
      const { data } = await axios.get(
        "https://react.aroosi.vip/api/data/getInfo.php"
      );
      setInfo(data.data.main);
      setLoading(false);
    } catch (error) {
      console.log("error", error);
    }
  };
  const RenderPage = () => {
    let returnValue = <Loading />;
    if (info && !loading) {
      returnValue = (
        <>
          <Header header={info} />
          {children}
          <Footer footer={info} />
        </>
      );
    }
    return returnValue;
  };
  useEffect(() => {
    const initial = setTimeout(() => {
      init();
    }, 50);
    return () => {
      clearTimeout(initial);
    };
  }, []);

  return <>{RenderPage()}</>;
};
export default Layout;
