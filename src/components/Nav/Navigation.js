import { Link } from "react-router-dom";
import Styles from "./Navigation.module.css";
import logo from "../../assets/images/logo.png";
import {
  FaTelegramPlane,
  FaInstagram,
  FaYoutube,
  FaWhatsapp,
  FaEnvelope,
} from "react-icons/fa";
const Navigation = ({ header }) => {
  const items = [
    { name: "خانه", to: "/", id: 1 },
    { name: "خدمات", to: "/service", id: 2 },
    { name: "بلاگ", to: "/blog", id: 3 },
    { name: "نوبت دهی", to: "/appointment", id: 4 },
    { name: "گالری", to: "/gallery", id: 5 },
    { name: "ارتباط با ما", to: "/about-us", id: 6 },
  ];
  return (
    <nav className={`${Styles.navigation} row text-center`}>
      <div className="col col-md-3 col-sm-12 col-12 col-lg-2 col-xl-2 col-xxl-2 d-md-none d-sm-none d-none d-lg-block text-center">
        {header.instagram && (
          <a href={header.instagram}>
            <span className={Styles.icon}>
              <FaInstagram />
            </span>
          </a>
        )}
        {header.email && (
          <a href={`mailto:${header.email}`}>
            <span className={Styles.icon}>
              <FaEnvelope />
            </span>
          </a>
        )}
        {header.whatsapp && (
          <a
            href={`${"https://api.whatsapp.com/send/?phone=98"}${header.whatsapp.slice(
              1
            )} `}
          >
            <span className={Styles.icon}>
              <FaWhatsapp />
            </span>
          </a>
        )}
        {header.telegram && (
          <a href={header.telegram}>
            <span className={Styles.icon}>
              <FaTelegramPlane />
            </span>
          </a>
        )}
        {header.youtube && (
          <a href={header.youtube}>
            <span className={Styles.icon}>
              <FaYoutube />
            </span>
          </a>
        )}
      </div>
      <div className="col col-md-6 col-sm-12 col-12 col-lg-8 col-xl-8 col-xxl-8 d-md-none d-sm-none d-none d-lg-block">
        <div className="row">
          {items.map((item) => {
            return (
              <div key={item.id} className="col">
                <Link to={item.to}>{item.name}</Link>
              </div>
            );
          })}
        </div>
      </div>
      <div className="col col-md-12 col-sm-12 col-12 col-lg-2 col-xl-2 col-xxl-2 text-center">
        <span className={Styles.title}>
          <Link to="/">{header.title}</Link>
        </span>
      </div>
    </nav>
  );
};
export default Navigation;
