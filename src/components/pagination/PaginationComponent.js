import Styles from "./Pagination.module.css";
import { FaAngleRight, FaAngleLeft } from "react-icons/fa";

const PaginationComponent = (props) => {
  const { max, current, changePaginate } = props;

  const changeValue = (page) => {
    props.history.push({
      pathname: props.location.pathname,
      search: `?page=${page}`,
    });
    changePaginate(page);
  };
  return (
    <div className={Styles.pagination} dir="ltr">
      <div
        className={`${Styles.item} ${Number(current) === 1 && Styles.disable}`}
        onClick={() => changeValue(Number(current) - 1)}
      >
        <FaAngleLeft />
      </div>

      {Number(current) > 1 && (
        <div
          onClick={() => changeValue(Number(current - 1))}
          className={Styles.item}
        >
          {Number(current) - 1}
        </div>
      )}
      <div
        onClick={() => changeValue(Number(current))}
        className={`${Styles.item} ${Styles.active}`}
      >
        {Number(current)}
      </div>
      {Number(current) < max && (
        <div
          onClick={() => changeValue(Number(current + 1))}
          className={Styles.item}
        >
          {Number(current) + 1}
        </div>
      )}
      <div
        className={`${Styles.item} ${
          Number(current) === Number(max) && Styles.disable
        }`}
        onClick={() => changeValue(Number(current) + 1)}
      >
        <FaAngleRight />
      </div>
    </div>
  );
};
export default PaginationComponent;
