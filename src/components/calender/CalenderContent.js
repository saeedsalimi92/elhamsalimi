import React, { useState } from "react";

/*import { Calendar } from "react-persian-datepicker";*/
/*import DatePicker from "react-datepicker2";*/
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import moment from "jalali-moment";
import "./Calneder.css";
const toCalendarType = (weekStartDay) =>
  weekStartDay === 0 ? "Arabic" : "ISO 8601";

const CalenderContent = ({ onSelectedDate }) => {
  var date = new Date();
  date.setDate(date.getDate() - 1);
  const [selectedDay, setSelectedDay] = useState(moment());
  const [today, setToDay] = useState(moment(date));

  const onClickDay = (day) => {
    onSelectedDate(day.selectedDay);
    setSelectedDay(day.selectedDay);
  };
  return (
    <div className="row col-12  justify-content-center calender-view ">
      <div className="col-12 col">
        <Calendar
          onClickDay={(selectedDay) => onClickDay({ selectedDay })}
          selectedDay={selectedDay}
          locale={"fa-ir"}
          minDate={new Date()}
          calendarType={toCalendarType(0)}
          className="react-calendar m-auto"
        />
      </div>
    </div>
  );
};

export default CalenderContent;
