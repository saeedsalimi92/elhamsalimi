import Navigation from "../Nav/Navigation";
import Styles from "./Header.module.css";
import { useState } from "react";
import logo from "../../assets/images/logo.png";

import {
  FaBars,
  FaEnvelope,
  FaInstagram,
  FaMapMarkerAlt,
  FaPhone,
  FaRegClock,
  FaTelegramPlane,
  FaWhatsapp,
  FaYoutube,
  FaAngleLeft,
} from "react-icons/fa";
import { Link } from "react-router-dom";

const Header = ({ header }) => {
  const items = [
    { name: "خانه", to: "/", id: 1 },
    { name: "خدمات", to: "/service", id: 2 },
    { name: "بلاگ", to: "/blog", id: 3 },
    { name: "نوبت دهی", to: "/appointment", id: 4, hasNew: true, new: "جدید" },
    { name: "گالری", to: "/gallery", id: 5 },
    { name: "ارتباط با ما", to: "/about-us", id: 6 },
  ];
  const [isShow, setIsShow] = useState(false);
  return (
    <header className={Styles.header}>
      <div
        className={`${Styles.slider} ${
          isShow && Styles.show
        } d-block d-sm-block d-md-block d-lg-none d-xl-none d-xxl-none `}
        onClick={() => setIsShow(!isShow)}
      >
        <div className={Styles.img}>
          <img className={Styles.logoImage} src={logo} />
        </div>
        <div className={Styles.logo}>{header.title}</div>
        <div className={Styles.subtitle}>
          مرکز مشاوره و خدمات روانشناختی پرواز
        </div>

        {items.map((item) => {
          return (
            <Link key={item.id} to={item.to}>
              <div className={Styles.item}>
                {item.hasNew ? (
                  <>
                    {item.name}
                    <div>
                      <span className={Styles.new}>{item.new}</span>
                      <FaAngleLeft />
                    </div>
                  </>
                ) : (
                  <>
                    <span>{item.name}</span>
                    <FaAngleLeft />
                  </>
                )}
              </div>
            </Link>
          );
        })}
        <div className={Styles.about}>
          <div className={Styles.aboutItem}>
            <FaPhone />

            <span className={Styles.icon}>
              <a href={`tel:+9821${header.phone}`}>{`${header.phone} - `}</a>
              <a href={`tel:+98${header.mobile.slice(1)}`}>{header.mobile}</a>
            </span>
          </div>
          <div className={Styles.aboutItem}>
            <FaMapMarkerAlt />
            <a href="https://goo.gl/maps/e7buEA77rQfcWiCg7">
              <span className={Styles.icon}>{header.address}</span>
            </a>
          </div>
          <div className={Styles.aboutItem}>
            <FaRegClock />
            <span className={Styles.icon}>
              شنبه تا پنج شنبه از ساعت 14 الی 20
            </span>
          </div>

          <div>
            <span className={Styles.icons}>
              <a href={header.instagram}>
                <FaInstagram />
              </a>
            </span>
            <span className={Styles.icons}>
              <a href={`mailto:${header.email}`}>
                <FaEnvelope />
              </a>
            </span>
            <span className={Styles.icons}>
              <a
                href={`${"https://api.whatsapp.com/send/?phone=98"}${header.whatsapp.slice(
                  1
                )}`}
              >
                <FaWhatsapp />
              </a>
            </span>
            <span className={Styles.icons}>
              <a href={header.telegram}>
                <FaTelegramPlane />
              </a>
            </span>
            <span className={Styles.icons}>
              <a href={header.youtube}>
                <FaYoutube />
              </a>
            </span>
          </div>
        </div>
      </div>

      <div
        onClick={() => setIsShow(!isShow)}
        className={`${Styles.menu} d-block d-sm-block d-md-block d-lg-none d-xl-none d-xxl-none pointer`}
      >
        <FaBars />
      </div>
      <div
        className={`${Styles.over} ${
          !isShow && Styles.hide
        } d-block d-sm-block d-md-block d-lg-none d-xl-none d-xxl-none`}
        onClick={() => setIsShow(!isShow)}
      ></div>
      <Navigation header={header} />
    </header>
  );
};
export default Header;
