import Styles from "./AboutComponentBox.module.css";

const AboutComponentBox = ({ aboutUs }) => {
  const renderDescription = () => {
    const div = document.createElement("div");
    div.innerHTML = aboutUs.description;
    return div.innerText;
  };
  return (
    <div className={Styles.back}>
      <div
        className={`${Styles.box} row col-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 col-sm-12 margin-lr-auto text-center`}
      >
        <div
          className={`${Styles.item} col col-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 col-sm-12 `}
        >
          <div className={Styles.title}>درباره ما</div>
          <div className={Styles.description}>{renderDescription()}</div>
        </div>
        <div
          className={`${Styles.item} col col-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 col-sm-12 d-md-none d-lg-block d-none `}
        >
          <div className={Styles.image}></div>
        </div>
      </div>
    </div>
  );
};
export default AboutComponentBox;
