import { Link } from "react-router-dom";
import Styles from "./Fotter.module.css";
import {
  FaEnvelope,
  FaInstagram,
  FaTelegramPlane,
  FaWhatsapp,
  FaYoutube,
  FaMapMarkerAlt,
  FaRegClock,
  FaPhone,
} from "react-icons/fa";
const Footer = ({ footer }) => {
  return (
    <footer className={Styles.footer}>
      <div className="row col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 margin-lr-auto align-items-center text-center">
        <div className="col ">
          <Link to="/">خانه</Link>
        </div>
        <div className="col ">
          <Link to="/service">خدمات</Link>
        </div>
        <div className="col ">
          <Link to="/blog">بلاگ</Link>
        </div>
        <div
          className={`${Styles.name} col col-md-12 col-sm-12 col-12 col-lg-3 col-xl-3 col-xxl-3`}
        >
          <Link to="/"> الهام سلیمی</Link>
        </div>
        <div className="col  ">
          <Link to="/appointment">نوبت دهی</Link>
        </div>
        <div className="col  ">
          <Link to="/gallery">گالری</Link>
        </div>
        <div className="col  ">
          <Link to="/about-us">درباره ما</Link>
        </div>
      </div>
      <div className={Styles.break}></div>
      <div className="row col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 margin-lr-auto">
        <div
          className={`${Styles.item} col col-md-12 col-sm-12 col-12 col-lg-8 col-xl-8 col-xxl-8 `}
        >
          <FaPhone />

          <span className={Styles.icon}>
            <a href={`tel:+9821${footer.phone}`}>{`${footer.phone} - `}</a>
            <a href={`tel:+98${footer.mobile.slice(1)}`}>{footer.mobile}</a>
          </span>
        </div>
        <div
          className={`${Styles.item} col col-md-12 col-sm-12 col-12 col-lg-4 col-xl-4 col-xxl-4`}
        >
          <FaRegClock />
          <span className={Styles.icon}>
            شنبه تا پنج شنبه از ساعت 14 الی 20
          </span>
        </div>
        <div
          className={`${Styles.item} col col-md-12 col-sm-12 col-12 col-lg-8 col-xl-8 col-xxl-8 `}
        >
          <FaMapMarkerAlt />
          <a href="https://goo.gl/maps/e7buEA77rQfcWiCg7">
            <span className={Styles.icon}>{footer.address}</span>
          </a>
        </div>
        <div
          className={`${Styles.item} col col-md-12 col-sm-12 col-12 col-lg-4 col-xl-4 col-xxl-4`}
        >
          <span className={Styles.icons}>
            <a href={footer.instagram}>
              <FaInstagram />
            </a>
          </span>
          <span className={Styles.icons}>
            <a href={`mailto:${footer.email}`}>
              <FaEnvelope />
            </a>
          </span>
          <span className={Styles.icons}>
            <a
              href={`${"https://api.whatsapp.com/send/?phone=98"}${footer.whatsapp.slice(
                1
              )}`}
            >
              <FaWhatsapp />
            </a>
          </span>
          <span className={Styles.icons}>
            <a href={footer.telegram}>
              <FaTelegramPlane />
            </a>
          </span>
          <span className={Styles.icons}>
            <a href={footer.youtube}>
              <FaYoutube />
            </a>
          </span>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
