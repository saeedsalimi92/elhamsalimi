import Styles from "./DetailPage.module.css";
import image from "../../assets/images/logo.png";
import {
  FaEnvelope,
  FaInstagram,
  FaTelegramPlane,
  FaWhatsapp,
  FaYoutube,
} from "react-icons/fa";
import { Link } from "react-router-dom";

const DetailPage = ({ item, items, routeName }) => {
  const renderDescription = () => {
    const div = document.createElement("div");
    div.innerHTML = item.description;
    return div.innerText;
  };

  return (
    <div
      className={`${Styles.box} row col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 col-xxl-10 margin-lr-auto`}
    >
      <div
        className={`${Styles.section} col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8`}
      >
        <div>
          <img
            className={Styles.image}
            src={item?.images[0] ? item.images[0].src : image}
            alt={item.title}
          />
        </div>
        <div className={Styles.title}>{item.title}</div>
        <div className={Styles.description}>{renderDescription()}</div>
        <div className={Styles.detail}>
          نوشته شده در : {item.created_time.slice(0, 10)}
        </div>
      </div>
      <div
        className={`${Styles.joinUsSection} col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4`}
      >
        {/*<div className={Styles.joinUsTitle}>به ما بپینودید</div>
        <div className={Styles.joinUsItem}>
          <FaInstagram />
        </div>
        <div className={Styles.joinUsItem}>
          <FaEnvelope />
        </div>
        <div className={Styles.joinUsItem}>
          <FaWhatsapp />
        </div>
        <div className={Styles.joinUsItem}>
          <FaTelegramPlane />
        </div>
        <div className={Styles.joinUsItem}>
          <FaYoutube />
        </div>*/}
        <div className={Styles.joinUsTitle}>پست های اخیر</div>
        {items.slice(0, 4).map((i) => {
          return (
            <div key={i.id} className={Styles.joinUsItem}>
              <Link to={`/${routeName}/${i.id}`}>{i.title}</Link>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default DetailPage;
