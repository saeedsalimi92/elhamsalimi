import Styles from "./Loading.module.css";
import image from "../../assets/images/loading.gif";
const Loading = () => {
  return (
    <div className={Styles.loading}>
      <img src={image} alt="" />
    </div>
  );
};
export default Loading;
