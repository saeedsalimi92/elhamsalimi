// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import Styles from "./SwiperComponent.module.css";
import SwiperCore, { Pagination, EffectFade, Autoplay, A11y } from "swiper";

// install Swiper modules

// Import Swiper styles
import "swiper/swiper.scss";
import "swiper/components/autoplay";
import "swiper/components/pagination/pagination.scss";
import "swiper/components/a11y/a11y.scss";
import "swiper/components/effect-fade/effect-fade.scss";

SwiperCore.use([Autoplay]);
// import Swiper core and required modules

const SwiperContainer = ({ sliders }) => {
  return (
    <Swiper
      modules={[Pagination, A11y, EffectFade, Autoplay]}
      pagination={{ clickable: true }}
      loop={true}
      effect="fade"
      autoplay={{
        delay: 5000,
      }}
      slidesPerView={1}
      onSlideChange={() => ""}
      onSwiper={() => ""}
    >
      {sliders.map((slider) => {
        return (
          <SwiperSlide key={slider.id}>
            <div>
              <img className={Styles.image} src={slider.images[0].src} alt="" />
              <div className={`${Styles.title} col col-12 text-center`}>
                {slider.title}
              </div>
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};
export default SwiperContainer;
