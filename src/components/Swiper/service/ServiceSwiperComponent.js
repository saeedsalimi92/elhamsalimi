// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import Styles from "./ServiceSwiperComponent.module.css";
import SwiperCore, { Pagination, EffectFade, Autoplay, A11y } from "swiper";
import "swiper/swiper.scss";
import "swiper/components/autoplay";
import "swiper/components/pagination/pagination.scss";
import "swiper/components/a11y/a11y.scss";
import "swiper/components/effect-fade/effect-fade.scss";
import ServiceItem from "../../services/serviceBoxComponent/ServiceItem/ServiceItem";

const ServiceSwiperContainer = ({ sliders }) => {
  return (
    <Swiper
      modules={[Pagination, A11y, EffectFade, Autoplay]}
      pagination={{ clickable: true }}
      loop={true}
      effect="fade"
      spaceBetween={50}
      slidesPerView={1}
      onSlideChange={() => ""}
      onSwiper={() => ""}
    >
      {sliders.map((slider) => {
        return (
          <SwiperSlide key={slider.id}>
            <div className="col col-12 col-sm-9 m-auto">
              <ServiceItem service={slider} />
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};
export default ServiceSwiperContainer;
