import BlogItem from "./BlogItem/BlogItem";
import Styles from "./BlogComponentBox.module.css";
import { Link } from "react-router-dom";
import { FaAngleLeft } from "react-icons/fa";
import BlogSwiperComponent from "../../Swiper/blog/BlogSwiperComponent";

const BlogComponentBox = ({ blog }) => {
  return (
    <div
      className={`${Styles.box}  col-12 col-md-12 col-lg-10 col-xxl-10 col-xl-10 col-sm-12 margin-lr-auto`}
    >
      <div className={Styles.titleSection}>
        <div className={Styles.title}>آخرین مطالب </div>
        <div className={Styles.more}>
          <Link to="/blog">
            بیشتر
            <FaAngleLeft />
          </Link>
        </div>
      </div>
      <div className="row justify-content-center d-none d-sm-none d-md-flex d-lg-flex ">
        {blog.slice(0, 3).map((item) => {
          return (
            <div
              className="col col-md-6 col-lg-4 col-xxl-4 col-xl-4 col-sm-9 col-12"
              key={item.id}
            >
              <BlogItem item={item} />
            </div>
          );
        })}
      </div>
      <div className="row justify-content-center d-flex d-sm-flex d-md-none d-lg-none d-xl-none d-xxl-none ">
        <div className=" col col-12">
          <BlogSwiperComponent sliders={blog.slice(0, 3)} />
        </div>
      </div>
    </div>
  );
};
export default BlogComponentBox;
