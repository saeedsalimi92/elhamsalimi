import { Link } from "react-router-dom";
import Styles from "./BlogItem.module.css";
import image from "../../../../assets/images/logo.png";

const BlogItem = ({ item }) => {
  return (
    <Link to={`/blog/${item.id}`}>
      <div className={Styles.card}>
        <img
          className={Styles.image}
          src={item?.images[0] ? item.images[0].src : image}
          alt={item.title}
        />
        <div className={Styles.date}>{item.created_time}</div>
        <div className={`${Styles.title} ${Styles.giveMeEllipsis}`}>
          {item.title}
        </div>
        <div>
          <button className={Styles.button}>بیشتر</button>
        </div>
      </div>
    </Link>
  );
};
export default BlogItem;
