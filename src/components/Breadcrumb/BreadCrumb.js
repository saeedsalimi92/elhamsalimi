import Styles from "./BreadCrumb.module.css";
import { Link } from "react-router-dom";
const BreadCrumb = ({ name, item, routeName }) => {
  return (
    <div className={Styles.back}>
      <div className={Styles.title}>{item.title}</div>
      <div className={Styles.breadCrumbList}>
        <div className={Styles.item}>
          <Link to="/"> خانه </Link>
        </div>
        <span className={Styles.item}> / </span>
        <div className={Styles.item}>
          <Link to={`/${routeName}`}> {name} </Link>
        </div>
        <span className={Styles.item}> / </span>
        <div className={Styles.active}>{item.title}</div>
      </div>
    </div>
  );
};
export default BreadCrumb;
