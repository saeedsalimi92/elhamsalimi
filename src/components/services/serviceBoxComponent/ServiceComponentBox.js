import ServiceItem from "./ServiceItem/ServiceItem";
import Styles from "./ServiceComponentBox.module.css";
import { FaAngleLeft } from "react-icons/fa";
import { Link } from "react-router-dom";
import ServiceSwiperContainer from "../../Swiper/service/ServiceSwiperComponent";
const ServiceComponentBox = ({ services }) => {
  return (
    <div
      className={`${Styles.box} col-12 col-md-12 col-lg-10 col-xxl-10 col-xl-10 col-sm-12 margin-lr-auto`}
    >
      <div className={Styles.titleSection}>
        <div className={Styles.title}>خدمات</div>
        <div className={Styles.more}>
          <Link to="/service">
            بیشتر
            <FaAngleLeft />
          </Link>
        </div>
      </div>
      <div className="row justify-content-center d-none d-sm-none d-md-flex d-lg-flex d-xl-flex d-xxl-flex ">
        {services.slice(0, 3).map((service) => {
          return (
            <div
              key={service.id}
              className="col col-12 col-md-6 col-lg-4 col-xxl-4 col-xl-4 col-sm-9 "
            >
              <ServiceItem service={service} />;
            </div>
          );
        })}
      </div>
      <div className="row justify-content-center d-flex d-sm-flex d-md-none d-lg-none d-xl-none d-xxl-none ">
        <div className="col col-12">
          <ServiceSwiperContainer sliders={services.slice(0, 3)} />
        </div>
      </div>
    </div>
  );
};
export default ServiceComponentBox;
