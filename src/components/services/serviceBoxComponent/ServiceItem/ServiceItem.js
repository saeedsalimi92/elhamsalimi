import { Link } from "react-router-dom";
import Styles from "./ServiceItem.module.css";
import image from "../../../../assets/images/logo.png";
const ServiceItem = ({ service }) => {
  return (
    <div className={Styles.item}>
      <Link to={`/service/${service.id}`}>
        <img
          className={Styles.image}
          src={service?.images[0] ? service.images[0].src : image}
          alt={service.title}
        />
      </Link>
      <Link to={`/service/${service.id}`}>
        <div>{service.title}</div>
      </Link>
    </div>
  );
};
export default ServiceItem;
