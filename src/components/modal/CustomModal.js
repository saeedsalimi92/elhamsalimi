import { Button } from "react-bootstrap";
import { Modal } from "react-bootstrap";
import Styles from "./CustomModal.module.css";
import SeparatorOfNumbers from "../../utils/SeparatorOfNumbers";

const CustomModal = (props) => {
  const acceptHandler = () => {
    props.onAccept(true);
  };
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dir="ltr"
      className="text-center"
      backdrop="static"
      onEscapeKeyDown={() => false}
    >
      <Modal.Header className="d-block">
        <Modal.Title id="contained-modal-title-vcenter" className="text-center">
          ثبت نهایی جلسه مشاوره
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className={Styles.item}>
          <span className={Styles.span}>نوع مشاوره :</span>
          {props.type === "attendance"
            ? " حضوری "
            : props.type === "phone"
            ? " تلفنی "
            : " اینترنتی (تماس تصویری یا صوتی) "}
        </div>
        <div className={Styles.item}>
          <span className={Styles.span}>نام مراجعه کننده :</span>
          {props.name}
        </div>

        <div className={Styles.item}>
          <span className={Styles.span}>تاریخ :</span>
          {props.persianDate}
        </div>

        <div className={Styles.item}>
          <span className={Styles.span}> ساعت : </span>
          {`${props.hours}:00`}
        </div>

        <div className={Styles.item}>
          <span className={Styles.span}> مبلغ پرداختی : </span>
          {`${SeparatorOfNumbers(props.price)}  ریال  `}
        </div>

        <div className={Styles.item}>
          <span className={Styles.span}>نوع پرداخت :</span>
          {props.payType === "online"
            ? " درگاه اینترنتی (ID PAY)"
            : " پرداخت  کارت به کارت"}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div className="d-flex w-100 justify-content-center">
          <div className="w-40">
            <Button
              disabled={props.onDisabled}
              className="w-100 btn-danger"
              onClick={props.onHide}
            >
              بازگشت
            </Button>
          </div>
          <div className="w-60">
            <Button
              dir="rtl"
              className="w-100 btn-success"
              onClick={() => acceptHandler()}
              disabled={props.onDisabled}
            >
              {props.onLoading
                ? "لطفا منتظر بمانید..."
                : " تایید نهایی و پرداخت"}
            </Button>
          </div>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
export default CustomModal;
