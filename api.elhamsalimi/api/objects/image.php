<?php



class Image
{

    // database connection and table name
    private $conn;
    private $table_name = "images";
    

    // object properties
    public $id;
    public $name;
    public $status;





    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    //post create method
    function create()
    {

        // query to insert record of new post create
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, status=:status";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->status = htmlspecialchars(strip_tags($this->status));
       

        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":status", $this->status);

        // execute query
        if ($stmt->execute()) {
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }

        return false;

    }
    
     function getById()
    {

        // query to insert record of new post create
         $query = "SELECT *
FROM
        " . $this->table_name . " WHERE id='" . $this->id . "' ORDER BY id DESC ";
          $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }
        return false;

    }


    function update()
    {
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                   name=:name, status=:status
                   
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize

        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->status = htmlspecialchars(strip_tags($this->status));
    

        // bind values
        $stmt->bindParam(":name", $this->name);
     
        $stmt->bindParam(":status", $this->status);


        // execute query
        if ($stmt->execute()) {
            return $this->id;
        }

        return false;

    }



    
    function delete()
    {
        $query = "DELETE
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='" . $this->id . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        
      
        return $stmt;
    }
}