<?php



class User
{

    // database connection and table name
    private $conn;
    private $table_name = "users";
    private $order_table_name = "orders";



    // object properties
    public $id;
    public $phone;
    public $email;
    public $name;
    public $user_type;
    public $password;
    public $token;
    public $otp;
    public $is_login;
    public $login_time;
    public $expire_time;
    public $register_type;
    public $receive_normal;
    public $receive_vip;
    public $order_id;
    public $active_order;
    public $admin_type;




    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

     function create()
    {
        
        if($this->existPhone()){
            return false;
        }
    
        // query to insert record of new blog create
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    phone=:phone,otp=:otp,user_type=:user_type,token=:token,password=:password ,register_type=:register_type,is_login=:is_login,login_time=:login_time,expire_time=:expire_time,order_id=:order_id ,active_order=:active_order,admin_type=:admin_type,receive_normal=:receive_normal,receive_vip=:receive_vip,name=:name";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->receive_normal = htmlspecialchars(strip_tags($this->receive_normal));
        $this->receive_vip = htmlspecialchars(strip_tags($this->receive_vip));
        $this->otp = htmlspecialchars(strip_tags($this->otp));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->user_type = htmlspecialchars(strip_tags($this->user_type));
        $this->admin_type = htmlspecialchars(strip_tags($this->admin_type));
        $this->token = htmlspecialchars(strip_tags($this->token));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->register_type = htmlspecialchars(strip_tags($this->register_type));
        $this->is_login = htmlspecialchars(strip_tags($this->is_login));
        $this->login_time = htmlspecialchars(strip_tags($this->login_time));
        $this->expire_time = htmlspecialchars(strip_tags($this->expire_time));
        $this->order_id = htmlspecialchars(strip_tags($this->order_id));
        $this->active_order = htmlspecialchars(strip_tags($this->active_order));
        
      
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":receive_normal", $this->receive_normal);
        $stmt->bindParam(":receive_vip", $this->receive_vip);
        $stmt->bindParam(":otp", $this->otp);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":user_type", $this->user_type);
        $stmt->bindParam(":admin_type", $this->admin_type);
        $stmt->bindParam(":token", $this->token);
        $stmt->bindParam(":register_type", $this->register_type);
        $stmt->bindParam(":login_time", $this->login_time);
        $stmt->bindParam(":expire_time", $this->expire_time);
        $stmt->bindParam(":is_login", $this->is_login);
        $stmt->bindParam(":order_id", $this->order_id);
        $stmt->bindParam(":active_order", $this->active_order);
     
        if ($stmt->execute()) {
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;

    }

    function update()
    {

        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    phone=:phone,otp=:otp,user_type=:user_type,token=:token,password=:password ,register_type=:register_type,is_login=:is_login,login_time=:login_time,expire_time=:expire_time,order_id=:order_id,active_order=:active_order,admin_type=:admin_type
                    ,receive_normal=:receive_normal,receive_vip=:receive_vip,name=:name
                WHERE
                   
                id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
         $this->phone = htmlspecialchars(strip_tags($this->phone));
         $this->receive_normal = htmlspecialchars(strip_tags($this->receive_normal));
        $this->receive_vip = htmlspecialchars(strip_tags($this->receive_vip));
        $this->otp = htmlspecialchars(strip_tags($this->otp));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->user_type = htmlspecialchars(strip_tags($this->user_type));
        $this->admin_type = htmlspecialchars(strip_tags($this->admin_type));
        $this->token = htmlspecialchars(strip_tags($this->token));
        $this->register_type = htmlspecialchars(strip_tags($this->register_type));
        $this->is_login = htmlspecialchars(strip_tags($this->is_login));
        $this->login_time = htmlspecialchars(strip_tags($this->login_time));
        $this->expire_time = htmlspecialchars(strip_tags($this->expire_time));
        $this->order_id = htmlspecialchars(strip_tags($this->order_id));
        $this->active_order = htmlspecialchars(strip_tags($this->active_order));
        $this->name = htmlspecialchars(strip_tags($this->name));
        
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":receive_normal", $this->receive_normal);
        $stmt->bindParam(":receive_vip", $this->receive_vip);
        $stmt->bindParam(":otp", $this->otp);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":user_type", $this->user_type);
        $stmt->bindParam(":admin_type", $this->admin_type);
        $stmt->bindParam(":token", $this->token);
        $stmt->bindParam(":register_type", $this->register_type);
        $stmt->bindParam(":login_time", $this->login_time);
        $stmt->bindParam(":expire_time", $this->expire_time);
        $stmt->bindParam(":is_login", $this->is_login);
        $stmt->bindParam(":order_id", $this->order_id);
        $stmt->bindParam(":active_order", $this->active_order);



        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;

    }

    function existPhone()
    {
        $query = "SELECT id,phone,user_type,register_type
            FROM
                " . $this->table_name . " 
            WHERE
                phone='" . $this->phone . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
             $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;
    }

    function loginByPassword()
    {
        $query = "SELECT
        id 
        FROM
          " . $this->table_name . "
          WHERE
          phone='" . $this->phone . "' AND password='" . $this->password . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
             $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;


    }
    function loginEmailByPassword()
    {
        $query = "SELECT
        id 
        FROM
          " . $this->table_name . "
          WHERE
          email='" . $this->email . "' AND password='" . $this->password . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
           $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;


    }
    function loginByOTP()
    {

        $query = "SELECT
        id 
        FROM
          " . $this->table_name . "
          WHERE
          phone='" . $this->phone . "' AND otp='" . $this->otp . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
           $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }

    function profile()
    {

        $query = "SELECT
                    id ,phone,user_type,token,login_time,expire_time,is_login,active_order,admin_type,receive_vip,receive_normal
                FROM
                    " . $this->table_name . " 
                WHERE
            id='" . $this->id . "' AND token LIKE'" . $this->token . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }
    
     function getId()
    {

        $query = "SELECT
                    id
                FROM
                    " . $this->table_name . " 
                WHERE
            phone='" . $this->phone . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }
    function getUserInfo()
    {

        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
                WHERE
            id='" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }
    function getById()
    {

        $query = "SELECT
                    id,phone,token,user_type,register_type,otp,active_order,order_id,admin_type,receive_vip,receive_normal
                FROM
                    " . $this->table_name . " 
                WHERE
            id='" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }
    function addOrderId()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    order_id=:order_id
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->order_id = htmlspecialchars(strip_tags($this->order_id));

        $stmt->bindParam(":order_id", $this->order_id);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }
    function setActiveOrder()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    active_order=:active_order
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->active_order = htmlspecialchars(strip_tags($this->active_order));

        $stmt->bindParam(":active_order", $this->active_order);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }
    function addUserType()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    user_type=:user_type
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->user_type = htmlspecialchars(strip_tags($this->user_type));

        $stmt->bindParam(":user_type", $this->user_type);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
             
            return $stmt;
        }

        return false;

    }
    function setToken()
    {

         $query = "UPDATE
                    " . $this->table_name . "
                SET
                   token=:token
                WHERE
                   
                id='" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
        function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
    }
     $this->token = sha1(uniqid($this->phone, true));
        
         $stmt->bindParam(":token", $this->token);
        // execute query
       // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;

    }
    function setOtp()
    {

         $query = "UPDATE
                    " . $this->table_name . "
                SET
                   otp=:otp
                WHERE
                   
                id='" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
    
        $this->otp = rand(1000,10000);
        
         $stmt->bindParam(":otp", $this->otp);
        // execute query
       // execute query
        if ($stmt->execute()) {
            return $this->id;
        }

        return false;

    }
    
    
    function getOrderById($id)
    {

        $query = "SELECT *
FROM
        " . $this->order_table_name . " WHERE id='" . $id . "' ORDER BY id DESC ";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }
        return false;
    }
    
    
    
    function getAll()
    {

        $query = "SELECT id,phone,user_type,order_id,receive_normal,receive_vip FROM " . $this->table_name . " ORDER BY id ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
       
        if ($stmt->rowCount() > 0) {
    
            function stmp($id,$phone,$user_type,$order_id,$receive_normal,$receive_vip) {
                $obj = new ArrayObject();
                $obj['id'] = $id;
                $obj['phone'] = $phone;
                $obj['user_type'] = $user_type;
                $obj['order_id'] = $order_id;
                $obj['receive_normal'] = $receive_normal;
                $obj['receive_vip'] = $receive_vip;
                $obj['orders'] = array();
                return $obj;
            }
            $Post = $stmt->fetchAll(PDO::FETCH_FUNC, "stmp");
              foreach ($Post as $row) {
                $orders = array();
                $row['orders'] = $orders;
                
                    $order_array = explode(".",$row['order_id']);
                    $order_array = \array_diff($order_array, [""]);
                    foreach ($order_array as $value) {
                        $order_stmt = $this->getOrderById($value);
                        if ($order_stmt) {
                            $order_object = $order_stmt->fetch(PDO::FETCH_ASSOC);
                            
                            array_push($orders, $order_object);
                        }
                    }
                    $row['orders'] = $orders;
                
            }
            return $Post;
        }
        return false;

    }
    
     function getUser()
    {

        $query = "SELECT
                    id,phone,user_type,order_id
                FROM
                    " . $this->table_name . " 
                WHERE
            id='" . $this->id . "'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
       
        if ($stmt->rowCount() > 0) {
    
        
            $Post = $stmt->fetch(PDO::FETCH_ASSOC);
            $orders = array();
            $Post['orders'] = $orders;
                
                    $order_array = explode(".",$Post['order_id']);
                    $order_array = \array_diff($order_array, [""]);
                    foreach ($order_array as $value) {
                        $order_stmt = $this->getOrderById($value);
                        if ($order_stmt) {
                            $order_object = $order_stmt->fetch(PDO::FETCH_ASSOC);
                            
                            array_push($orders, $order_object);
                        }
                    }
                    $Post['orders'] = $orders;
            
            return $Post;
        }
        return false;

    }
    function delete()
    {
        $query = "DELETE
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='" . $this->id . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}