<?php



class Reserve
{

  // database connection and table name
  private $conn;
  private $table_name = "reserve";


  // object properties
  public $id;
  public $order_id;
  public $status;
  public $name;
  public $phone;
  public $time;
  public $date;
  public $payType;
  public $isPay;





  // constructor with $db as database connection
  public function __construct($db)
  {
    $this->conn = $db;
  }

  //post create method
  function create()
  {
    if($this->checkOrder()){
        return false;
    }
    if($this->checkTime()){
        return false;
    }

    // query to insert record of new post create
    $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    order_id=:order_id, status=:status ,
                    phone=:phone,name=:name,date=:date,time=:time,payType=:payType,isPay=:isPay
                    ";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->order_id = htmlspecialchars(strip_tags($this->order_id));
    $this->status = htmlspecialchars(strip_tags($this->status));
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->phone = htmlspecialchars(strip_tags($this->phone));
    $this->time = htmlspecialchars(strip_tags($this->time));
    $this->date = htmlspecialchars(strip_tags($this->date));
    $this->payType = htmlspecialchars(strip_tags($this->payType));
     $this->isPay = htmlspecialchars(strip_tags($this->isPay));
    


    // bind values
    $stmt->bindParam(":order_id", $this->order_id);
    $stmt->bindParam(":status", $this->status);
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":phone", $this->phone);
    $stmt->bindParam(":time", $this->time);
    $stmt->bindParam(":date", $this->date);
    $stmt->bindParam(":payType", $this->payType);
     $stmt->bindParam(":isPay", $this->isPay);

    // execute query
    if ($stmt->execute()) {
      $this->id = $this->conn->lastInsertId();
      return $this->id;
    }

    return false;

  }
function checkOrder(){
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE order_id='" . $this->order_id . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;
}   
function checkTime(){
     $query = "SELECT *
FROM
        " . $this->table_name . " WHERE date='" . $this->date . "' ORDER BY id DESC ";
    /*$stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }*/
    return false;
 
}


  function getById()
  {

    // query to insert record of new post create
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE id='" . $this->id . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;

  }
  function getByOrderId()
  {

    // query to insert record of new post create
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE order_id='" . $this->order_id . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;

  }
  function getByDate()
  {

    // query to insert record of new post create
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE date='" . $this->date . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      function reserve($id,$date,$time,$name,$phone,$order_id,$status,$payType,$isPay) {
                $obj = new ArrayObject();
                $obj['id'] = $id;
                $obj['date'] = $date;
                $obj['time'] = $time;
                $obj['name'] = $name;
                $obj['phone'] = $phone;
                $obj['order_id'] = $order_id;
                $obj['status'] = $status;
                $obj['payType'] = $payType;
                $obj['isPay'] = $isPay;
                return $obj;
            }
            $Post = $stmt->fetchAll(PDO::FETCH_FUNC, "reserve");
        
            return $Post;
    }
    return false;
  }
  function getByDateForClient()
  {

    // query to insert record of new post create
    $query = "SELECT id,date,time,status,payType,isPay
FROM
        " . $this->table_name . " WHERE date='" . $this->date . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      function reserve2($id,$date,$time,$status,$payType,$isPay) {
                $obj = new ArrayObject();
                $obj['id'] = $id;
                $obj['date'] = $date;
                $obj['time'] = $time;
                $obj['status'] = $status;
                $obj['payType'] = $payType;
                $obj['isPay'] = $isPay;
                return $obj;
            }
            $Post = $stmt->fetchAll(PDO::FETCH_FUNC, "reserve2");
        
            return $Post;
    }
    return false;
  }
  
  
 function getByDateAndTime()
  {

    // query to insert record of new post create
    $query = "SELECT id,date,time
FROM
        " . $this->table_name . " WHERE date='" . $this->date . "' AND time='".$this->time. "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;

  }

  function update()
  {
    $query = "UPDATE
                    " . $this->table_name . "
                SET
                    order_id=:order_id, status=:status ,
                    phone=:phone,name=:name,date=:date,time=:time,payType=:payType,isPay=:isPay

                WHERE id='" . $this->id . "'";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize

   
    // sanitize
    $this->order_id = htmlspecialchars(strip_tags($this->order_id));
    $this->status = htmlspecialchars(strip_tags($this->status));
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->phone = htmlspecialchars(strip_tags($this->phone));
    $this->time = htmlspecialchars(strip_tags($this->time));
    $this->date = htmlspecialchars(strip_tags($this->date));
    $this->payType = htmlspecialchars(strip_tags($this->payType));
    $this->isPay = htmlspecialchars(strip_tags($this->isPay));
    
    


    // bind values
    $stmt->bindParam(":order_id", $this->order_id);
    $stmt->bindParam(":status", $this->status);
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":phone", $this->phone);
    $stmt->bindParam(":time", $this->time);
    $stmt->bindParam(":date", $this->date);
    $stmt->bindParam(":payType", $this->payType);
    $stmt->bindParam(":isPay", $this->isPay);



    // execute query
    if ($stmt->execute()) {
      return $this->id;
    }

    return false;

  }





  function delete()
  {
    $query = "DELETE
                FROM
                    " . $this->table_name . "
                WHERE
                    id='" . $this->id . "'";
    // prepare query statement
    $stmt = $this->conn->prepare($query);
    // execute query
    $stmt->execute();


    return $stmt;
  }
}
