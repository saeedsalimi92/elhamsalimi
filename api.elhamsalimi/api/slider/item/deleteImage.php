<?php
// get database connection
include_once '../../config/database.php';

// instantiate user object

include_once '../../objects/slider-item.php';
include_once '../../objects/image.php';

$database = new Database();
$db = $database->getConnection();


$data = new SliderItem($db);
$media = new Image($db);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$media->id =  $_POST['image_id'];
$data->id =  $_POST['id'];
$image =  $_POST['name'];
$filepath = "../../../images/" . $image;

// create the blog
$stmt = $media->delete();
if($stmt){
    
    if (is_file($filepath))
    {
        unlink($filepath);
    }
    $Post = $data->getById(false);

    

    $image_array = explode(".",$Post['image_id']);
    $image_array = \array_diff($image_array, [""]); 
    $image_array = \array_diff($image_array, [$media->id]); 

    $image_array = implode(".",$image_array);

    $data->image_id =  $image_array ;
    
    $data_stmt = $data->addImage();

    
    http_response_code(200);
    $data_arr=array(
        "status" => true,
        "message" => "آیتم با موفقیت حدف شد!",
        "data" => ''
    );
}
else{
    http_response_code(400);
    $data_arr=array(
        "status" => false,
        "message" => "حذف آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
        "data" => null
    );
}

print_r(json_encode($data_arr));
?>