<?php

// get database connection
include_once '../../config/database.php';

include_once '../../objects/slider-item.php';

$database = new Database();
$db = $database->getConnection();

$data = new SliderItem($db);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}

$haveImage = true;
$stmt = $data->getAll($haveImage);

if ($stmt) {
    $data_arr = array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $stmt
    );

} else {
    http_response_code(404);
    $data_arr = array(
        "status" => false,
        "message" => "آیتم مورد نظر یافت نشد!",
        "data" => null
    );
}

print_r(json_encode($data_arr));
?>