<?php
 
// get database connection
include_once '../../config/database.php';
 
// instantiate user object
include_once '../../objects/blog-item.php';
 


$database = new Database();
$db = $database->getConnection();

$data = new BlogItem($db);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$data->id =  $_POST['id'];

$haveImage = false;
$Post = $data->getById($haveImage);


if($Post){
    // get retrieved row

$data->title = isset($_POST['title']) ? $_POST['title'] : $Post['title'];
$data->en_title = isset($_POST['en_title']) ? $_POST['en_title'] : $Post['en_title'];
$data->subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : $Post['subtitle'];
$data->en_subtitle = isset($_POST['en_subtitle']) ? $_POST['en_subtitle'] : $Post['en_subtitle'];
$data->description = isset($_POST['description']) ? $_POST['description'] : $Post['description'];
$data->en_description = isset($_POST['en_description']) ? $_POST['en_description'] : $Post['en_description'];
$data->status = isset($_POST['status']) ? $_POST['status'] : $Post['status'];
$data->image_id = isset($_POST['image_id']) ? $_POST['image_id'] : $Post['image_id'];
$data->created_time = date("Y-m-d H:i:s");
$data->media_link = isset($_POST['media_link']) ? $_POST['media_link'] : $Post['media_link'];


    $stmt = $data->update();
    if($stmt){
        http_response_code(200);
        $data_arr=array(
            "status" => true,
            "message" => "آیتم با موفیقت بروزرسانی شد!",
            "data" => null
        );
    }
    else{
        http_response_code(400);
        $data_arr=array(
            "status" => false,
            "message" => "بروزرسانی آیتم با مشکل روبرو شد لطفا بعداامتحان فرمایید!",
            "data" => null
        );
    }

}

// create the blog
print_r(json_encode($data_arr));
?>