<?php
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user.php';
include_once '../../objects/package.php';

$database = new Database();
$db = $database->getConnection();
 

$user = new User($db);
$package = new Package($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}
$hasError = false;
$user->id = $_GET['u_id'] ;
$post = $user->getById();


$link ='';
$package->product_id = $_GET['s_id'];
$pack = $package->getByProduct();
$userChacked = false;
$link ='';
if($_GET['c_id'] == 'n'){
    if($post['receive_normal'] == '1'){
        
      $userChacked = true  ;
    }

}else if($_GET['c_id'] == 'v'){
    
    if($post['receive_vip'] == '1'){
        
         $userChacked = true;
    }
      
}else{
    $hasError = true;
}





if($hasError){
    http_response_code(400);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک منقضی است',
        "data" => null
    );
}else if($userChacked){
    http_response_code(403);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک قبلا استفاده شده است',
        "data" => null
    );
}else{
    http_response_code(200);
    $user_arr=array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => ''
    );
}


print_r(json_encode($user_arr));


?>