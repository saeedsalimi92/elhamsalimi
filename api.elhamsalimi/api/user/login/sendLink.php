<?php
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user.php';
include_once '../../objects/package.php';

$database = new Database();
$db = $database->getConnection();
 

$user = new User($db);
$package = new Package($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}
$hasError = false;
$user->id = $_POST['id'] ;
$data = $user->getById();
$package->product_id = $_POST['s_id'];
$pack = $package->getByProduct();
$userChacked = false;
$link ='';
if($_POST['s_id'] == '1'){
    if($data['receive_normal'] == '1'){
      $userChacked = true  ;
    }
    $link = $pack['link']. '?u_id=' . $data['id'] . '&ty=n';
    
}else if($_POST['s_id'] == '2'){
    
    if($data['receive_vip'] == '0'){
         $userChacked = true  ;
    }
     $link = $pack['link']. '?u_id=' . $data['id'] . '&ty=v';
}else{
    $hasError = true;
}



if($hasError){
    http_response_code(400);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک منقضی است',
        "data" => null
    );
}else if($userChacked){
    http_response_code(403);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک قبلا استفاده شده است',
        "data" => null
    );
}else{
    http_response_code(200);
    $user_arr=array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $link
    );
}





print_r(json_encode($user_arr));


?>