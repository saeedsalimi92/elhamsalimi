<?php
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user.php';
include_once '../../objects/package.php';

$database = new Database();
$db = $database->getConnection();
 

$user = new User($db);
$package = new Package($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}
$hasError = false;
$user->id = $_GET['u_id'] ;
$client = $_GET['client'] ;

$Post = $user->getUserInfo();

$user->phone =  $Post['phone'];
$user->otp =  $Post['otp'];
$user->token =  $Post['token'];
$user->password =  $Post['password'];
$user->user_type =  $Post['user_type'];
$user->admin_type =  $Post['admin_type'];
$user->register_type =  $Post['register_type'];
$user->is_login =  $Post['is_login'];
$user->login_time =  $Post['login_time'];
$user->expire_time =  $Post['expire_time'];
$user->order_id =  $Post['order_id'];
$user->active_order =  $Post['active_order'];
$user->receive_normal = $Post['receive_normal'];
$user->receive_vip =  $Post['receive_vip'];
$link ='';
$package->product_id = $_GET['s_id'];
$pack = $package->getByProduct();
$userChacked = false;
$link ='';
if($client == md5($Post['phone'])){
    if($_GET['c_id'] == 'n'){
    if($Post['receive_normal'] == '1'){
        
      $userChacked = true  ;
    }else{
        
       $user->receive_normal = '1'; 
       $user_data = $user->update();
       $link = $pack['link'];
    }

}else if($_GET['c_id'] == 'v'){
    
    if($Post['receive_vip'] == '1'){
        
         $userChacked = true;
    }else{
        
      $user->receive_vip = '1'; 
      $user_data = $user->update();
      $link = $pack['link'];  
    }
      
}else{
    $hasError = true;
}
}else{
     $hasError = true;
}








if($hasError){
    http_response_code(400);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک منقضی است',
        "data" => null
    );
}else if($userChacked){
    http_response_code(403);
    $user_arr=array(
        "status" => false,
        "message" => 'لینک قبلا استفاده شده است',
        "data" => null
    );
}else{
    http_response_code(200);
    $user_arr=array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $link
    );
}


print_r(json_encode($user_arr));


?>