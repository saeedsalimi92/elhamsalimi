<?php
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user.php';


// get database connection
$database = new Database();
$db = $database->getConnection();



 
// prepare user object
$user = new User($db);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}


 

 
$user->id = $_GET['id'];

$Post = $user->getUserInfo();


$user->phone = isset($_GET['phone']) ? $_GET['phone'] : $Post['phone'];
$user->otp = '';
$user->token = '';
$user->password = $Post['password'];
$user->user_type = isset($_GET['user_type']) ? $_GET['user_type'] : $Post['user_type'];
$user->admin_type = $Post['admin_type'];
$user->register_type = isset($_GET['register_type']) ? $_GET['register_type'] : $Post['register_type'];
$user->is_login =  '0';
$user->login_time =  '';
$user->expire_time =  '';
$user->order_id = isset($_GET['order_id']) ? $_GET['order_id'] : $Post['order_id'];
$user->receive_normal = isset($_GET['receive_normal']) ? $_GET['receive_normal'] : $Post['receive_normal'];
$user->receive_vip = isset($_GET['receive_vip']) ? $_GET['receive_vip'] : $Post['receive_vip'];
$user->active_order = '0';
$user->name = $Post['name'];




// read the details of user to be edited
$stmt = $user->update();
// set ID property of user to be edited
// create array
if($stmt){
http_response_code(200);
    $user_arr = array(
        "status" => true,
        "message" => "با موفقیت خارج شدید",
        "data" =>null
    );
}else{
    http_response_code(400);

    $user_arr=array(
        "status" => true,
        "message" => "مشکلی در خروج از حساب کاربری پیش آمده, لطفا مجددا امتحان کنید",
        "data" => null
    );
}



print_r(json_encode($user_arr));
?>