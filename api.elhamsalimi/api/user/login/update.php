<?php
// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user.php';



// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare user object
$user = new User($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}

// set ID property of user to be edited
$user->id = $_POST['id'];

$Post = $user->getUserInfo();


$user->phone = isset($_POST['phone']) ? $_POST['phone'] : $Post['phone'];
$user->otp = isset($_POST['otp']) ? $_POST['otp'] : $Post['otp'];
$user->token = isset($_POST['token']) ? $_POST['token'] : $Post['token'];
$user->password = isset($_POST['password']) ? base64_encode($_POST['password']) : $Post['password'];
$user->user_type = isset($_POST['user_type']) ? $_POST['user_type'] : $Post['user_type'];
$user->admin_type =  $Post['admin_type'];
$user->register_type = isset($_POST['register_type']) ? $_POST['register_type'] : $Post['register_type'];
$user->is_login = isset($_POST['is_login']) ? $_POST['is_login'] : $Post['is_login'];
$user->login_time = isset($_POST['login_time']) ? $_POST['login_time'] : $Post['login_time'];
$user->expire_time = isset($_POST['expire_time']) ? $_POST['expire_time'] : $Post['expire_time'];
$user->order_id = isset($_POST['order_id']) ? $_POST['order_id'] : $Post['order_id'];
$user->active_order = isset($_POST['active_order']) ? $_POST['active_order'] : $Post['active_order'];
$user->receive_normal = isset($_POST['receive_normal']) ? $_POST['receive_normal'] : $Post['receive_normal'];
$user->receive_vip = isset($_POST['receive_vip']) ? $_POST['receive_vip'] : $Post['receive_vip'];

// read the details of user to be edited
$stmt = $user->update();
if($stmt){
    http_response_code(200);
    $user_arr = array(
        "status" => true,
        "message" => "بروز رسانی اطلاعات با موفقیت انجام شد!",
        "data" =>null
    );
}else{

    http_response_code(400);
    $user_arr=array(
        "status" => true,
        "message" => "مشکلی در بروز رسانی پیش امده است لطفا بعدا امتحان فرمایید",
        "data" =>null
    );


}


print_r(json_encode($user_arr));
// make it json format

?>