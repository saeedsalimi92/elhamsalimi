<?php
 
// get database connection
include_once '../../config/database.php';
 
// instantiate user object
include_once '../../objects/user.php';


 
$database = new Database();
$db = $database->getConnection();
 
$user = new User($db);
$user->id = $_GET['id'];


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}



    $stmt = $user->getUserInfo();
    if($stmt){
        // get retrieved row
      
        // create array
        http_response_code(200);
        $user_arr=array(
            "status" => true,
            "message" => "عملیات با موفقیت انجام شد",
            'data' => $stmt
        );

    }
    else{
        http_response_code(404);
       $user_arr=array(
            "status" => false,
            "message" => "مشکلی پیش آمده است ، لطفا بعدا امتحان فرمایید",
            'data' => ''
        );
    }



print_r(json_encode($user_arr));
?>