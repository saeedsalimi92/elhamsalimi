<?php

// get database connection
include_once '../config/database.php';

// instantiate user object
include_once '../objects/user.php';


$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// set blog property values
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$user->id = $_POST['id'];


$stmt = $user->delete();
if ($stmt) {
    http_response_code(200);
    $data_arr = array(
        "status" => true,
        "message" => "آیتم با موفقیت حدف شد!",
        "data" => null
    );
} else {
    http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "حذف آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
        "data" => null
    );
}
print_r(json_encode($data_arr));
?>