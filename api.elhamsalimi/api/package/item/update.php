<?php
 
// get database connection
include_once '../../config/database.php';
 
// instantiate user object
include_once '../../objects/package.php';
 


$database = new Database();
$db = $database->getConnection();

$data = new Package($db);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$data->id =  $_POST['id'];

$haveImage = false;
$Post = $data->getById();


if($Post){
    // get retrieved row

$data->link = isset($_POST['link']) ? $_POST['link'] : $Post['link'];
$data->valid = isset($_POST['valid']) ? $_POST['valid'] : $Post['valid'];
$data->product_id = isset($_POST['product_id']) ? $_POST['product_id'] : $Post['product_id'];



    $stmt = $data->update();
    if($stmt){
        http_response_code(200);
        $data_arr=array(
            "status" => true,
            "message" => "آیتم با موفیقت بروزرسانی شد!",
            "data" => null
        );
    }
    else{
        http_response_code(400);
        $data_arr=array(
            "status" => false,
            "message" => "بروزرسانی آیتم با مشکل روبرو شد لطفا بعداامتحان فرمایید!",
            "data" => null
        );
    }

}

// create the blog
print_r(json_encode($data_arr));
?>