<?php

include_once '../config/database.php';
include_once '../objects/service.php';

$database = new Database();
$db = $database->getConnection();

$data = new Service($db);




$data->id = $_GET['id'];
$haveImage = true;

$stmt = $data->getById($haveImage);

if ($stmt) {

    http_response_code(200);
    $data_arr = array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $stmt,
    );

} else {
    http_response_code(404);
    $data_arr = array(
        "status" => false,
        "message" => "آیتم مورد نظر یافت نشد!",
        "data" => null
    );
}


print_r(json_encode($data_arr));
?>