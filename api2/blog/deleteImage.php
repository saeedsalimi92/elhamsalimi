<?php
// get database connection
include_once '../config/database.php';

// instantiate user object
include_once '../objects/image.php';
include_once '../objects/blog.php';

$database = new Database();
$db = $database->getConnection();

$media = new Image($db);
$data = new Blog($db);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$media->id =  $_POST['image_id'];
$data->id =  $_POST['id'];
$type =  $_POST['type'];

$image =  $_POST['name'];
$file = "../../images/" . $image;
// create the blog
$stmt = $media->delete();
if($stmt){
    
      if (is_file($file))
    {
        unlink($file);
    }
 
    if($type === 'over'){
        
        $data->over_id =  '0';
    }else{
        $data->back_id =  '0';
    }

    
    $data_stmt = $data->addImage();

    
    http_response_code(200);
    $data_arr=array(
        "status" => true,
        "message" => "آیتم با موفقیت حدف شد!",
        "data" => ''
    );
}
else{
    http_response_code(400);
    $data_arr=array(
        "status" => false,
        "message" => "حذف آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
        "data" => null
    );
}

print_r(json_encode($data_arr));
?>