<?php
include_once '../config/database.php';
include_once '../objects/information.php';

$database = new Database();
$db = $database->getConnection();

$data = new Information($db);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}


$data->id = $_GET['id'];


$stmt = $data->getById();

if ($stmt) {

    http_response_code(200);
    $data_arr = array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $stmt,
    );

} else {
    http_response_code(404);
    $data_arr = array(
        "status" => false,
        "message" => "آیتم مورد نظر یافت نشد!",
        "data" => null
    );
}


print_r(json_encode($data_arr));
?>