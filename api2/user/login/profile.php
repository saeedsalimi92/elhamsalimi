<?php
 
// get database connection
include_once '../../config/database.php';
 
// instantiate user object
include_once '../../objects/user.php';


 
$database = new Database();
$db = $database->getConnection();
 
$user = new User($db);
$user->id = $_POST['id'];
$user->token = $_POST['token'];

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}



    $stmt = $user->profile();
    if($stmt){
        // get retrieved row
      
        // create array
        http_response_code(200);
        $user_arr=array(
            "status" => true,
            "message" => "کاربر در سیستم لاگین است",
            'data' => $stmt
        );

    }
    else{
        http_response_code(403);
       $user_arr=array(
            "status" => true,
            "message" => "کاربر لاگین نیست",
            'data' => ''
        );
    }



print_r(json_encode($user_arr));
?>