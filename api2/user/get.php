<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare user object
$user = new User($db);

// set ID property of user to be edited
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $user->id = $_GET['id'] ;
        break;
    case 'POST':
        $user->id = $_POST['id'] ;
        break;
    default:
}



// read the details of user to be edited
$stmt = $user->getUserInfo();
if($stmt){
      
    http_response_code(200);
    $user_arr = array(
                "status" => false,
                "message" => 'عملیات با موفقیت انجام شد',
                "data" => $stmt
            );
}else{

    http_response_code(400);
    $user_arr = array(
                "status" => false,
                "message" =>'کاربر مورد نظر پیدا نشد',
                "data" => $user
            );;


}


print_r(json_encode($user_arr));


?>