<?php

// get database connection
include_once '../config/database.php';

// instantiate user object
include_once '../objects/footer.php';
include_once '../objects/image.php';


$database = new Database();
$db = $database->getConnection();

$data = new Footer($db);
$media = new Image($db);

// set blog property values
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$data->id = $_POST['id'];


$image_stmt = $data->getById(true);


foreach ($image_stmt['images'] as $value) {
    $media->id = $value['id'];
    $image = $value['name'];
    $file = "../../images/" . $image;
    $media_stmt = $media->delete();
    if ($media_stmt) {
        if (is_file($file)) {
            unlink($file);
        }
    }
}

$stmt = $data->delete();
if ($stmt) {
    http_response_code(200);
    $data_arr = array(
        "status" => true,
        "message" => "آیتم با موفقیت حدف شد!",
        "data" => null
    );
} else {
    http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "حذف آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
        "data" => null
    );
}
print_r(json_encode($data_arr));
?>