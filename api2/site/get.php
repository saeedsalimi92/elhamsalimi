<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/site.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare user object
$user = new Site($db);

// set ID property of user to be edited

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $user->id = $_GET['site_id'] ;
        break;
    case 'POST':
        $user->id = $_POST['site_id'] ;
        break;
    default:
}



// read the details of user to be edited
$stmt = $user->getInformation();
if($stmt){
      class Obj {}
      $stmt->setFetchMode(PDO::FETCH_CLASS, 'Obj');
      $user = $stmt->fetch();
    http_response_code(200);
    $user_arr = array(
                "status" => false,
                "message" => 'عملیات با موفقیت انجام شد',
                "data" => $user
            );
}else{

    http_response_code(400);
    $user_arr = array(
                "status" => false,
                "message" =>'کاربر مورد نظر پیدا نشد',
                "data" => $user
            );;


}


print_r(json_encode($user_arr));


?>