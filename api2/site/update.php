<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/site.php';



// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare user object
$user = new Site($db);


 

// set ID property of user to be edited
$user->id = $_POST['id'];
$user->title = $_POST['title'];
$user->email =$_POST['email'];
$user->phone = $_POST['phone'];
$user->address = $_POST['address'];
$user->whatsapp =$_POST['whatsapp'];
$user->instagram = $_POST['instagram'];
$user->telegram = $_POST['telegram'];
$user->about_us = $_POST['about_us'];
$user->fax = $_POST['fax'];
$user->mobile = $_POST['mobile'];
$user->description = $_POST['description'];
$user->subtitle = $_POST['subtitle'];



// read the details of user to be edited
$stmt = $user->update();
if($stmt){
    http_response_code(200);
    $user_arr = array(
        "status" => true,
        "message" => "بروز رسانی اطلاعات با موفقیت انجام شد!",
        "data" =>null
    );
}else{

    http_response_code(400);
    $user_arr=array(
        "status" => false,
        "message" => "مشکلی در بروز رسانی پیش امده است لطفا بعدا امتحان فرمایید",
        "data" =>null
    );


}


print_r(json_encode($user_arr));
// make it json format

?>