<?php

// get database connection
include_once '../config/database.php';

include_once '../objects/package.php';

$database = new Database();
$db = $database->getConnection();

$data = new Package($db);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}


$stmt = $data->getAll();

if ($stmt) {
    $data_arr = array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $stmt
    );

} else {
    http_response_code(404);
    $data_arr = array(
        "status" => false,
        "message" => "آیتم مورد نظر یافت نشد!",
        "data" => null
    );
}

print_r(json_encode($data_arr));
?>