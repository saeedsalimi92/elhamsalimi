<?php

// get database connection
include_once '../../config/database.php';

include_once '../../objects/package.php';

$database = new Database();
$db = $database->getConnection();

$data = new Package($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}

$hasError = false;



$data->link = $_POST['link'];
$data->product_id = $_POST['product_id'];
$data->valid = $_POST['valid'];


if ($hasError) {
    http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "پر کردن فیلد ها اجباری است",
     
        "data" => null
    );
} else {
    $stmt = $data->create();
    if (!$stmt) {
        http_response_code(400);
        $data_arr = array(
            "status" => false,
            "message" => "ثبت آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
            "data" => null
        );
    } else {
      
        $data->id = $stmt;
        $get_stmt = $data->getById();
        http_response_code(200);
        $data_arr = array(
            "status" => false,
            "message" => "عملیات با موفقیت انجام شد!",
            "data" => $get_stmt['id']
        );
    }
}
// create the data
print_r(json_encode($data_arr));

?>