<?php
// get database connection
include_once '../config/database.php';

// instantiate user object
include_once '../objects/image.php';
include_once '../objects/main.php';

$database = new Database();
$db = $database->getConnection();

$data = new Main($db);


$file_name = $_FILES["media"]["name"];
$file_type = $_FILES["media"]["type"];
$temp_name = $_FILES["media"]["tmp_name"];
$file_size = $_FILES["media"]["size"];

$target_dir = "../../images/";


$target_file = $target_dir . basename($file_name);
$name = basename($file_name);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));



// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($temp_name);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        http_response_code(400);
        $data_arr = array(
            "status" => false,
            "message" => "فایل ارسالی غیر مجار می باشد",
            "data" => null
        );
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    $temp = explode(".", $file_name);
    $newFileName = round(microtime(true)) . '.' . end($temp);
    $target_file = $target_dir . $newFileName;
    $name= $newFileName;
}
// Check file size
if ($file_size > 524200) {
    http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "حجم عکس ارسالی نباید بیشتر از 5 مگابایت باشد",
        "data" => null
    );
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"&& $imageFileType != "gif" ) {
    http_response_code(400);
   $data_arr = array(
        "status" => false,
        "message" => "فایل ارسالی مجاز نمی باشد",
        "data" => null
    );
}



// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    http_response_code(400);
  $data_arr = array(
        "status" => false,
        "message" => "متاسفیم عکس شما بارگذاری نشد!",
        "data" => null
    );
// if everything is ok, try to upload file
} else {

    if (move_uploaded_file($temp_name, $target_file)) {
        $media = new Image($db);
        $media->name = $name;
        $media->status = true;
        // create the media
        $stmt = $media->create();
        if (!$stmt) {
            http_response_code(400);
            $data_arr = array(
                "status" => false,
                "message" => "ثبت عکس با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
                "data" => null
            );
        }
        else {
            $data->id = $_POST['submit_id'];
            if($_POST['type'] == 'over'){
                $data->over_id =  $stmt ;
                $data_stmt = $data->addOverImage();
            }else{
                $data->back_id =  $stmt ;
                $data_stmt = $data->addBackImage();
            }
            if($data_stmt){
                http_response_code(200);
                $data_arr = array(
                    "status" => true,
                    "message" => "عملیات با موفقیت انجام شد",
                    "data" => $stmt
                );
            }else{
                http_response_code(400);
                $data_arr = array(
                    "status" => false,
                    "message" => "مشکلی پیش آمده لطفا بعدا امتحان فرمایید",
                    "data" => ''
                );
            }

        }
    } else {
        http_response_code(400);
        $data_arr = array(
            "status" => false,
            "message" => "متاسفیم عکس شما بارگذاری نشد!",
            "data" => null
        );
    }
}
print_r(json_encode($data_arr));
?>