<?php

class Database{
 
    // specify your own database credentials
    private $host = "localhost";            //Server
    private $db_name = "elhamsal_new";       //Database Name
    private $username = "elhamsal_admin";             //UserName of Phpmyadmin
    private $password = "aAdmin@12345";                 //Password associated with username
    public $conn;
 
    
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        
            $this->conn->exec("SET character_set_connection = 'UTF8");
        $this->conn->exec("SET NAMES 'UTF8");
        $this->conn->exec("SET CHARACTER SET 'utf8'");
         
        }catch(PDOException $exception){
            http_response_code(500);
            $post_arr = array(
                "status" => false,
                "message" => " مشکلی در ارتباط با سرور پیش امده است",
                "data" => null
            );
            print_r(json_encode($post_arr));
        }
 
        return $this->conn;
    }
}
?>