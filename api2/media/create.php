<?php

// get database connection
include_once '../config/database.php';


include_once '../objects/media.php';

$database = new Database();
$db = $database->getConnection();

$data = new Media($db);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}

$hasError = false;


if ($_POST['title'] == '' || $_POST['en_title'] == '' || $_POST['subtitle'] == '' || $_POST['en_subtitle'] == '' ||
    $_POST['description'] == '' || $_POST['en_description'] == '' || $_POST['status'] == '' ) {
    $hasError = true;
}




$data->title = $_POST['title'];
$data->en_title = $_POST['en_title'];
$data->subtitle = $_POST['subtitle'];
$data->en_subtitle = $_POST['en_subtitle'];
$data->description = $_POST['description'];
$data->en_description = $_POST['en_description'];
$data->status = $_POST['status'];
$data->media_link = $_POST['media_link'];
$data->back_id = '0';




if ($hasError) {
    http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "پر کردن فیلد ها اجباری است",
        "error" => $stmt->error,
        "data" => null
    );
} else {
    $stmt = $data->create();
    if (!$stmt) {
        http_response_code(400);
        $data_arr = array(
            "status" => false,
            "message" => "ثبت آیتم با مشکل روبرو شد لطفا بعدا امتحان فرمایید!",
            "data" => null
        );
    } else {
        $haveImage = true;
        $data->id = $stmt;
        $get_stmt = $data->getById($haveImage);
        http_response_code(200);
        $data_arr = array(
            "status" => false,
            "message" => "عملیات با موفقیت انجام شد!",
            "data" => $get_stmt['id']
        );
    }
}

// create the data
print_r(json_encode($data_arr));


?>