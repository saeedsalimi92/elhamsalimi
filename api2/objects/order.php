<?php



class Order
{

  // database connection and table name
  private $conn;
  private $table_name = "orders";


  // object properties
  public $id;
  public $order_id;
  public $track_id;
  public $verify_track_id;
  public $response_text;
  public $payment_id;
  public $card_no;
  public $verify_time;
  public $site_track_id;
  public $price;
  public $created_time;
  public $updated_time;
  public $status;
  public $name;
  public $phone;
  public $hours;
  public $date;
  public $res_link;
  public $res_id;
  public $type;
  public $payType;





  // constructor with $db as database connection
  public function __construct($db)
  {
    $this->conn = $db;
  }

  //post create method
  function create()
  {

    // query to insert record of new post create
    $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    order_id=:order_id, status=:status , price=:price,
                    track_id=:track_id ,site_track_id=:site_track_id, verify_track_id=:verify_track_id,
                    card_no=:card_no, payment_id=:payment_id, created_time=:created_time,
                    updated_time=:updated_time, verify_time=:verify_time, response_text=:response_text,
                     res_id=:res_id, res_link=:res_link,
                    type=:type,phone=:phone,name=:name,date=:date,hours=:hours,payType=:payType
                    ";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->order_id = htmlspecialchars(strip_tags($this->order_id));
    $this->price = htmlspecialchars(strip_tags($this->price));
    $this->track_id = htmlspecialchars(strip_tags($this->track_id));
    $this->verify_time = htmlspecialchars(strip_tags($this->verify_time));
    $this->verify_track_id = htmlspecialchars(strip_tags($this->verify_track_id));
    $this->payment_id = htmlspecialchars(strip_tags($this->payment_id));
    $this->site_track_id = htmlspecialchars(strip_tags($this->site_track_id));
    $this->created_time = htmlspecialchars(strip_tags($this->created_time));
    $this->updated_time = htmlspecialchars(strip_tags($this->updated_time));
    $this->res_id = htmlspecialchars(strip_tags($this->res_id));
    $this->res_link = htmlspecialchars(strip_tags($this->res_link));
    $this->response_text = htmlspecialchars(strip_tags($this->response_text));
    $this->card_no = htmlspecialchars(strip_tags($this->card_no));
    $this->status = htmlspecialchars(strip_tags($this->status));
    $this->type = htmlspecialchars(strip_tags($this->type));
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->phone = htmlspecialchars(strip_tags($this->phone));
    $this->hours = htmlspecialchars(strip_tags($this->hours));
    $this->date = htmlspecialchars(strip_tags($this->date));
    $this->payType = htmlspecialchars(strip_tags($this->payType));
  


    // bind values
    $stmt->bindParam(":order_id", $this->order_id);
    $stmt->bindParam(":track_id", $this->track_id);
    $stmt->bindParam(":payment_id", $this->payment_id);
    $stmt->bindParam(":created_time", $this->created_time);
    $stmt->bindParam(":updated_time", $this->updated_time);
    $stmt->bindParam(":card_no", $this->card_no);
    $stmt->bindParam(":response_text", $this->response_text);
    $stmt->bindParam(":site_track_id", $this->site_track_id);
    $stmt->bindParam(":verify_track_id", $this->verify_track_id);
    $stmt->bindParam(":verify_time", $this->verify_time);
    $stmt->bindParam(":price", $this->price);
    $stmt->bindParam(":status", $this->status);
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":phone", $this->phone);
    $stmt->bindParam(":hours", $this->hours);
    $stmt->bindParam(":date", $this->date);
    $stmt->bindParam(":res_id", $this->res_id);
    $stmt->bindParam(":type", $this->type);
    $stmt->bindParam(":res_link", $this->res_link);
    $stmt->bindParam(":payType", $this->payType);

    // execute query
    if ($stmt->execute()) {
      $this->id = $this->conn->lastInsertId();
      return $this->id;
    }

    return false;

  }



  function getById()
  {

    // query to insert record of new post create
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE id='" . $this->id . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;

  }
  function getByOrderId()
  {

    // query to insert record of new post create
    $query = "SELECT *
FROM
        " . $this->table_name . " WHERE order_id='" . $this->order_id . "' ORDER BY id DESC ";
    $stmt = $this->conn->prepare($query);

    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
      return $data;
    }
    return false;

  }


  function update()
  {
    $query = "UPDATE
                    " . $this->table_name . "
                SET
                    order_id=:order_id, status=:status , price=:price,
                    track_id=:track_id ,site_track_id=:site_track_id, verify_track_id=:verify_track_id,
                    card_no=:card_no, payment_id=:payment_id, created_time=:created_time,
                    updated_time=:updated_time, verify_time=:verify_time, response_text=:response_text,
                     res_id=:res_id, res_link=:res_link,
                    type=:type,phone=:phone,name=:name,date=:date,hours=:hours,payType=:payType

                WHERE id='" . $this->id . "'";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize

    $this->order_id = htmlspecialchars(strip_tags($this->order_id));
    $this->price = htmlspecialchars(strip_tags($this->price));
    $this->track_id = htmlspecialchars(strip_tags($this->track_id));
    $this->verify_time = htmlspecialchars(strip_tags($this->verify_time));
    $this->verify_track_id = htmlspecialchars(strip_tags($this->verify_track_id));
    $this->payment_id = htmlspecialchars(strip_tags($this->payment_id));
    $this->site_track_id = htmlspecialchars(strip_tags($this->site_track_id));
    $this->created_time = htmlspecialchars(strip_tags($this->created_time));
    $this->updated_time = htmlspecialchars(strip_tags($this->updated_time));
    $this->res_id = htmlspecialchars(strip_tags($this->res_id));
    $this->res_link = htmlspecialchars(strip_tags($this->res_link));
    $this->response_text = htmlspecialchars(strip_tags($this->response_text));
    $this->card_no = htmlspecialchars(strip_tags($this->card_no));
    $this->status = htmlspecialchars(strip_tags($this->status));
    $this->type = htmlspecialchars(strip_tags($this->type));
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->phone = htmlspecialchars(strip_tags($this->phone));
     $this->hours = htmlspecialchars(strip_tags($this->hours));
    $this->date = htmlspecialchars(strip_tags($this->date));
    $this->payType = htmlspecialchars(strip_tags($this->payType));
    



    // bind values
    $stmt->bindParam(":order_id", $this->order_id);
    $stmt->bindParam(":track_id", $this->track_id);
    $stmt->bindParam(":payment_id", $this->payment_id);
    $stmt->bindParam(":created_time", $this->created_time);
    $stmt->bindParam(":updated_time", $this->updated_time);
    $stmt->bindParam(":card_no", $this->card_no);
    $stmt->bindParam(":response_text", $this->response_text);
    $stmt->bindParam(":site_track_id", $this->site_track_id);
    $stmt->bindParam(":verify_track_id", $this->verify_track_id);
    $stmt->bindParam(":verify_time", $this->verify_time);
    $stmt->bindParam(":price", $this->price);
    $stmt->bindParam(":status", $this->status);
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":phone", $this->phone);
    $stmt->bindParam(":hours", $this->hours);
    $stmt->bindParam(":date", $this->date);
    $stmt->bindParam(":res_id", $this->res_id);
    $stmt->bindParam(":type", $this->type);
    $stmt->bindParam(":res_link", $this->res_link);
    $stmt->bindParam(":payType", $this->payType);



    // execute query
    if ($stmt->execute()) {
      return $this->id;
    }

    return false;

  }

  function setOrderId()
  {
    $query = "UPDATE
                    " . $this->table_name . "
                SET
                   order_id=:order_id

                WHERE id='" . $this->id . "'";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $id = (int)$this->id;
    $o_id = $id + 100000;
    $this->order_id =  $o_id;
    // bind values
    $stmt->bindParam(":order_id", $this->order_id);
    // execute query
    if ($stmt->execute()) {

      return $this->id;
    }

    return false;

  }




  function delete()
  {
    $query = "DELETE
                FROM
                    " . $this->table_name . "
                WHERE
                    id='" . $this->id . "'";
    // prepare query statement
    $stmt = $this->conn->prepare($query);
    // execute query
    $stmt->execute();


    return $stmt;
  }
}
