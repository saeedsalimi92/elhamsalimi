<?php



class Information
{

    // database connection and table name
    private $conn;
    private $table_name = "information";



    // object properties
    public $id;
    public $title;
    public $en_title;
    public $subtitle;
    public $en_subtitle;
    public $description;
    public $en_description;
    public $status;
    public $image_id;
    public $email;
    public $whatsapp;
    public $telegram;
    public $instagram;
    public $address;
    public $address_link;
    public $phone;
    public $mobile;
    public $facebook;
    public $youtube;
    public $aparat;




    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    function create()
    {

        
        // query to insert record of new blog create
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    title=:title,en_title=:en_title,subtitle=:subtitle,en_subtitle=:en_subtitle,description=:description,
                    en_description=:en_description,status=:status,image_id=:image_id , phone=:phone,email=:email,
                    telegram=:telegram , whatsapp=:whatsapp, instagram=:instagram,mobile=:mobile,address=:address,
                    aparat=:aparat,facebook=:facebook,youtube=:youtube,
                    address_link=:address_link";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->en_title = htmlspecialchars(strip_tags($this->en_title));
        $this->subtitle = htmlspecialchars(strip_tags($this->subtitle));
        $this->en_subtitle = htmlspecialchars(strip_tags($this->en_subtitle));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->en_description = htmlspecialchars(strip_tags($this->en_description));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->image_id = htmlspecialchars(strip_tags($this->image_id));
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->mobile = htmlspecialchars(strip_tags($this->mobile));
        $this->whatsapp = htmlspecialchars(strip_tags($this->whatsapp));
        $this->instagram = htmlspecialchars(strip_tags($this->instagram));
        $this->telegram = htmlspecialchars(strip_tags($this->telegram));
        $this->address = htmlspecialchars(strip_tags($this->address));
        $this->facebook = htmlspecialchars(strip_tags($this->facebook));
        $this->youtube = htmlspecialchars(strip_tags($this->youtube));
        $this->aparat = htmlspecialchars(strip_tags($this->aparat));
      


        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":en_title", $this->en_title);
        $stmt->bindParam(":subtitle", $this->subtitle);
        $stmt->bindParam(":en_subtitle", $this->en_subtitle);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":en_description", $this->en_description);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":image_id", $this->image_id);
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":mobile", $this->mobile);
        $stmt->bindParam(":whatsapp", $this->whatsapp);
        $stmt->bindParam(":telegram", $this->telegram);
        $stmt->bindParam(":instagram", $this->instagram);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":address", $this->address);
        $stmt->bindParam(":facebook", $this->facebook);
        $stmt->bindParam(":aparat", $this->aparat);
        $stmt->bindParam(":youtube", $this->youtube);
        $stmt->bindParam(":address_link", $this->address_link);

        if ($stmt->execute()) {
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;

    }

    function update()
    {

        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    title=:title,en_title=:en_title,subtitle=:subtitle,en_subtitle=:en_subtitle,description=:description,
                    en_description=:en_description,status=:status,image_id=:image_id , phone=:phone,email=:email,
                    telegram=:telegram , whatsapp=:whatsapp, instagram=:instagram,mobile=:mobile,address=:address,
                    aparat=:aparat,facebook=:facebook,youtube=:youtube,
                    address_link=:address_link
                WHERE
                   
                id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
     $this->title = htmlspecialchars(strip_tags($this->title));
        $this->en_title = htmlspecialchars(strip_tags($this->en_title));
        $this->subtitle = htmlspecialchars(strip_tags($this->subtitle));
        $this->en_subtitle = htmlspecialchars(strip_tags($this->en_subtitle));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->en_description = htmlspecialchars(strip_tags($this->en_description));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->image_id = htmlspecialchars(strip_tags($this->image_id));
        $this->phone = htmlspecialchars(strip_tags($this->phone));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->mobile = htmlspecialchars(strip_tags($this->mobile));
        $this->whatsapp = htmlspecialchars(strip_tags($this->whatsapp));
        $this->instagram = htmlspecialchars(strip_tags($this->instagram));
        $this->telegram = htmlspecialchars(strip_tags($this->telegram));
        $this->address = htmlspecialchars(strip_tags($this->address));
        $this->facebook = htmlspecialchars(strip_tags($this->facebook));
        $this->youtube = htmlspecialchars(strip_tags($this->youtube));
        $this->aparat = htmlspecialchars(strip_tags($this->aparat));
      


        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":en_title", $this->en_title);
        $stmt->bindParam(":subtitle", $this->subtitle);
        $stmt->bindParam(":en_subtitle", $this->en_subtitle);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":en_description", $this->en_description);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":image_id", $this->image_id);
        $stmt->bindParam(":phone", $this->phone);
        $stmt->bindParam(":mobile", $this->mobile);
        $stmt->bindParam(":whatsapp", $this->whatsapp);
        $stmt->bindParam(":telegram", $this->telegram);
        $stmt->bindParam(":instagram", $this->instagram);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":address", $this->address);
        $stmt->bindParam(":address_link", $this->address_link);
        $stmt->bindParam(":facebook", $this->facebook);
        $stmt->bindParam(":aparat", $this->aparat);
        $stmt->bindParam(":youtube", $this->youtube);



        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;

    }

 

    
    function getById()
    {

        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . " 
                WHERE
            id='" . $this->id . "'";

        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        return false;

    }

    function delete()
    {
        $query = "DELETE
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='" . $this->id . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}