<?php


class Package
{

    // database connection and table name
    private $conn;
    private $table_name = "package";
  


    // object properties
    public $id;
    public $link;
    public $valid;
    public $product_id;









    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    //data create method
    function create()
    {
        // query to insert record of new blog create
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    link=:link,product_id=:product_id, valid=:valid";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->link = htmlspecialchars(strip_tags($this->link));
        $this->product_id = htmlspecialchars(strip_tags($this->product_id));
        $this->valid = htmlspecialchars(strip_tags($this->valid));
    



        $stmt->bindParam(":link", $this->link);
        $stmt->bindParam(":product_id", $this->product_id);
        $stmt->bindParam(":valid", $this->valid);
    






        if ($stmt->execute()) {
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;

    }

    function update()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    link=:link,product_id=:product_id, valid=:valid
                   
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->link = htmlspecialchars(strip_tags($this->link));
        $this->product_id = htmlspecialchars(strip_tags($this->product_id));
        $this->valid = htmlspecialchars(strip_tags($this->valid));
    



        $stmt->bindParam(":link", $this->link);
        $stmt->bindParam(":product_id", $this->product_id);
        $stmt->bindParam(":valid", $this->valid);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }

    

 function getById()
    {
    
        $query = "SELECT *
FROM
        " . $this->table_name . " WHERE id='" . $this->id . "' ORDER BY id DESC ";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
            return $data;
        }
        return false;
    }
    
    function getByProduct()
    {
    
        $query = "SELECT *
FROM
        " . $this->table_name . " WHERE product_id='" . $this->product_id . "' ";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
            return $data;
        }
        return false;
    }



    function getAll()

    {
        $query = "SELECT * FROM " . $this->table_name . " ORDER BY id ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
    
            function link_stmp($id,$link,$valid,$product_id) {
                $obj = new ArrayObject();
                $obj['id'] = $id;
                $obj['link'] = $link;
             
                $obj['valid'] = $valid;
                   $obj['product_id'] = $product_id;
                
                return $obj;
            }
            $Post = $stmt->fetchAll(PDO::FETCH_FUNC, "link_stmp");
        
            return $Post;
        }
        return false;
    }

    function delete()
    {
        $query = "DELETE
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='" . $this->id . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}
?>