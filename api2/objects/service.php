<?php


class Service
{

    // database connection and table name
    private $conn;
    private $table_name = "service";
    private $table_image = "images";
    private $image_folder = '/images/';


    // object properties
    public $id;
    public $title;
    public $en_title;
    public $subtitle;
    public $en_subtitle;
    public $description;
    public $en_description;
    public $status;
    public $back_id;
    public $media_link;










    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    //data create method
    function create()
    {
        // query to insert record of new blog create
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    title=:title,en_title=:en_title, subtitle=:subtitle,en_subtitle=:en_subtitle, description=:description,
                    en_description=:en_description,status=:status,back_id=:back_id,media_link=:media_link";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->en_title = htmlspecialchars(strip_tags($this->en_title));
        $this->subtitle = htmlspecialchars(strip_tags($this->subtitle));
        $this->en_subtitle = htmlspecialchars(strip_tags($this->en_subtitle));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->back_id = htmlspecialchars(strip_tags($this->back_id));
        $this->media_link = htmlspecialchars(strip_tags($this->media_link));



        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":en_title", $this->en_title);
        $stmt->bindParam(":subtitle", $this->subtitle);
        $stmt->bindParam(":en_subtitle", $this->en_subtitle);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":en_description", $this->en_description);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":back_id", $this->back_id);
        $stmt->bindParam(":media_link", $this->media_link);







        if ($stmt->execute()) {
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;

    }

    function update()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    title=:title,en_title=:en_title, subtitle=:subtitle,en_subtitle=:en_subtitle, description=:description,
                    en_description=:en_description,status=:status,back_id=:back_id,media_link=:media_link
                   
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->en_title = htmlspecialchars(strip_tags($this->en_title));
        $this->subtitle = htmlspecialchars(strip_tags($this->subtitle));
        $this->en_subtitle = htmlspecialchars(strip_tags($this->en_subtitle));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->back_id = htmlspecialchars(strip_tags($this->back_id));
        $this->media_link = htmlspecialchars(strip_tags($this->media_link));



        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":en_title", $this->en_title);
        $stmt->bindParam(":subtitle", $this->subtitle);
        $stmt->bindParam(":en_subtitle", $this->en_subtitle);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":en_description", $this->en_description);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":back_id", $this->back_id);
        $stmt->bindParam(":media_link", $this->media_link);


        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }

    function addImage()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    image_id=:image_id
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->image_id = htmlspecialchars(strip_tags($this->image_id));

        $stmt->bindParam(":image_id", $this->image_id);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }
    function addBackImage()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    back_id=:back_id
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->back_id = htmlspecialchars(strip_tags($this->back_id));

        $stmt->bindParam(":back_id", $this->back_id);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }
    function addOverImage()
    {
        // query to insert record of new user signup
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    over_id=:over_id
               
                WHERE id='" . $this->id . "'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        // sanitize
        $this->over_id = htmlspecialchars(strip_tags($this->over_id));

        $stmt->bindParam(":over_id", $this->over_id);

        // execute query
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }

        return false;

    }

    function getById($hasImage)
    {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        $query = "SELECT *
FROM
        " . $this->table_name . " WHERE id='" . $this->id . "' ORDER BY id DESC ";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            $data['back_image']= null;
            if ($hasImage) {
                $media_stmt = $this->getImageById($data['back_id']);
                if ($media_stmt) {
                    $media_object = $media_stmt->fetch(PDO::FETCH_ASSOC);
                    $media_object['src'] = $url . $this->image_folder . $media_object['name'];

                    $data['back_image'] = $media_object;
                }
            }

            return $data;
        }
        return false;
    }

    function getImageById($id)
    {

        $query = "SELECT *
FROM
        " . $this->table_image . " WHERE id='" . $id . "' ORDER BY id DESC ";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            return $stmt;
        }
        return false;
    }

    function getAll($hasImage)

    {
        $query = "SELECT * FROM " . $this->table_name . " ORDER BY id ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            function stmp($id,$title,$en_title,$subtitle,$en_subtitle,$description,$en_description,$status ,$image_id) {
                $obj = new ArrayObject();
                $obj['id'] = $id;
                $obj['title'] = $title;
                $obj['en_title'] = $en_title;
                $obj['subtitle'] = $subtitle;
                $obj['en_subtitle'] = $en_subtitle;
                $obj['description'] = $description;
                $obj['en_description'] = $en_description;
                $obj['status'] = $status;
                $obj['image_id'] = $image_id;
                $obj['images'] = array();
                return $obj;
            }
            $Post = $stmt->fetchAll(PDO::FETCH_FUNC, "stmp");
            foreach ($Post as $row) {
                $images = array();
                $row['images'] = $images;
                if($hasImage){
                    $image_array = explode(".",$row['image_id']);
                    $image_array = \array_diff($image_array, [""]);
                    foreach ($image_array as $value) {
                        $media_stmt = $this->getImageById($value);
                        if ($media_stmt) {
                            $media_object = $media_stmt->fetch(PDO::FETCH_ASSOC);
                            $media_object['src'] = $url .$this->image_folder. $media_object['name'];
                            array_push($images, $media_object);
                        }
                    }
                    $row['images'] = $images;
                }
            }
            return $Post;
        }
        return false;
    }

    function delete()
    {
        $query = "DELETE
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='" . $this->id . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}
?>