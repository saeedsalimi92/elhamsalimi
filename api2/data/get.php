<?php
include_once '../config/database.php';
include_once '../objects/information.php';

include_once '../objects/service.php';
include_once '../objects/service-item.php';
include_once '../objects/blog.php';
include_once '../objects/blog-item.php';
include_once '../objects/slider.php';
include_once '../objects/slider-item.php';

$database = new Database();
$db = $database->getConnection();

$main = new Information($db);

$service = new Service($db);
$service_item = new ServiceItem($db);
$blog = new Blog($db);
$blog_item = new BlogItem($db);
$slider = new Slider($db);
$slider_item = new SliderItem($db);


switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        return [
            "status" => false
        ];
        break;
    case 'OPTIONS':
        http_response_code(204);
        return;
        break;
    default:
}

$haveImage = true;
$hasError = false;





$service->id = '1';
$service_stmt = $service->getById($haveImage);

$main->id = '1';
$main_stmt = $main->getById($haveImage);

$service_item_stmt = $service_item->getAll($haveImage);

$blog->id = '1';
$blog_stmt = $blog->getById($haveImage);

$blog_item_stmt = $blog_item->getAll($haveImage);

$slider->id = '1';
$slider_stmt = $slider->getById($haveImage);

$slider_item_stmt = $slider_item->getAll($haveImage);


$data = (Object) array(
    'main' => (Object) array(),
    
    'service' => (Object) array(),
    'service_items' => array(),
    'blog' => (Object) array(),
    'blog_items' => array(),
    'slider' => (Object) array(),
    'slider_items' => array(),
    );


if($main_stmt){
    $data->main = $main_stmt;
}else{
    $hasError = true;
}



if($service_stmt){
    $data->service = $service_stmt;
}else{
    $hasError = true;
}

if($service_item_stmt){
    $data->service_items = $service_item_stmt;
}else{

}


if($blog_stmt){
    $data->blog = $blog_stmt;
}else{
    $hasError = true;
}

if($blog_item_stmt){
    $data->blog_items = $blog_item_stmt;
}else{
 
}

if($slider_stmt){
    $data->slider = $slider_stmt;
}else{
    $hasError = true;
}

if($slider_item_stmt){
    $data->slider_items = $slider_item_stmt;
}else{
   
}


if ($hasError) {

  http_response_code(400);
    $data_arr = array(
        "status" => false,
        "message" => "آیتم مورد نظر یافت نشد!",
        "data" => null
    );

} else {
      http_response_code(200);
    $data_arr = array(
        "status" => true,
        "message" => 'عملیات با موفقیت انجام شد',
        "data" => $data,
    );
    
}


print_r(json_encode($data_arr));
?>