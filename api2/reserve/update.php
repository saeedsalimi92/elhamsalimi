<?php
 
// get database connection
include_once '../config/database.php';
 
// instantiate user object
include_once '../objects/reserve.php';
 


$database = new Database();
$db = $database->getConnection();

$data = new Reserve($db);
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        return [
            "status" => false
        ];
        break;
    case 'OPTION':
        http_response_code(204);
        return;
        break;
    default:
}

$data->id =  $_POST['id'];

$Post = $data->getById();


if($Post){
    // get retrieved row

$data->order_id = isset($_POST['order_id']) ? $_POST['order_id'] : $Post['order_id'];
$data->date = isset($_POST['date']) ? $_POST['date'] : $Post['date'];
$data->time = isset($_POST['time']) ? $_POST['time'] : $Post['time'];
$data->name = isset($_POST['name']) ? $_POST['name'] : $Post['name'];
$data->phone = isset($_POST['phone']) ? $_POST['phone'] : $Post['phone'];
$data->isPay = isset($_POST['isPay']) ? $_POST['isPay'] : $Post['isPay'];
$data->payType = isset($_POST['payType']) ? $_POST['payType'] : $Post['payType'];
$data->status = isset($_POST['status']) ? $_POST['status'] : $Post['status'];



    $stmt = $data->update();
    if($stmt){
        http_response_code(200);
        $data_arr=array(
            "status" => true,
            "message" => "آیتم با موفیقت بروزرسانی شد!",
            "data" => null
        );
    }
    else{
        http_response_code(400);
        $data_arr=array(
            "status" => false,
            "message" => "بروزرسانی آیتم با مشکل روبرو شد لطفا بعداامتحان فرمایید!",
            "data" => null
        );
    }

}

// create the blog
print_r(json_encode($data_arr));
?>